---
title: "Multi Crypto Wallet: for Bitcoin and 20 currencies"
altTitle: 

users: 500
appId: appinventor.ai_oldgoldmen.CryptoWallet
launchDate: 
latestUpdate: 2020-02-02
apkVersionName: "1.0"
stars: 4.8
ratings: 36
reviews: 15
size: 3.3M
website: 
repository: 
issue: 
icon: appinventor.ai_oldgoldmen.CryptoWallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /appinventor.ai_oldgoldmen.CryptoWallet/
---


