---
title: "Klever Wallet: Buy Bitcoin, Ethereum, Tron, Crypto"
altTitle: 

users: 100000
appId: cash.klever.blockchain.wallet
launchDate: 
latestUpdate: 2021-01-26
apkVersionName: "4.1.3"
stars: 4.2
ratings: 4630
reviews: 2540
size: Varies with device
website: https://www.tronwallet.me
repository: 
issue: 
icon: cash.klever.blockchain.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /cash.klever.blockchain.wallet/
---


We found this app as [TronWallet](/com.tronwallet2/)'s listing at Google named
this app's website as its website, too. Or actually both apps claim
`www.tronwallet.me` was their website but that page only redirects to
`klever.io/en/` which in turn only points to this app not to TronWallet.

This app's description looks like a copy of TronWallet's description with the
same strong claims about being non-custodial and also without a link to the source
code on Google Play. On the website we can't find a link to their source
code neither.
[Searching GitHub](https://github.com/search?q=%22cash.klever.blockchain.wallet%22&type=Code)
doesn't yield relevant results neither and we conclude that this wallet is
closed source and thus **not verifiable**.
