---
title: "Bitcoin Wallet"
altTitle: "Bitcoin Wallet by Bitcoin.com"

users: 1000000
appId: com.bitcoin.mwallet
launchDate: 2017-06-19
latestUpdate: 2021-01-13
apkVersionName: "6.9.10"
stars: 4.4
ratings: 17946
reviews: 6684
size: 47M
website: https://www.bitcoin.com
repository: https://github.com/Bitcoin-com/Wallet
issue: https://github.com/Bitcoin-com/Wallet/issues/39
icon: com.bitcoin.mwallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-27
reviewStale: true
signer: 
reviewArchive:
- date: 2019-12-20
  version: "5.13.3"
  apkHash: 
  gitRevision: fc6cffcbb02fa7d441528eec517e5d61050cf26c
  verdict: nonverifiable

providerTwitter: bitcoincom
providerLinkedIn: 
providerFacebook: buy.bitcoin.news
providerReddit: btc

redirect_from:
  - /com.bitcoin.mwallet/
  - /posts/com.bitcoin.mwallet/
---


According to
[the words of its owner](https://www.reddit.com/r/btc/comments/g04ece/bitcoincom_wallet_app_is_still_closed_source/fn7rlvy/)
this wallet is closed source until further notice. We assume it is still
supposed to be non-custodial but without source code, this is **not verifiable**.
