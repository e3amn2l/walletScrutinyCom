---
title: "OKEX - خرید بیت کوین"
altTitle: 

users: 10000
appId: co.okex.app
launchDate: 
latestUpdate: 2020-12-06
apkVersionName: "2.6.0"
stars: 3.9
ratings: 551
reviews: 243
size: 12M
website: https://ok-ex.co
repository: 
issue: 
icon: co.okex.app.png
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-16
reviewStale: false
signer: 
reviewArchive:
- date: 2020-11-16
  version: 
  apkHash: 
  gitRevision: bcb5dbfd724ca531c1965cce7ef0d38f023e4c0c
  verdict: custodial


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.okex.app/
---


**Update:** This app appears to have disappeared from Google Play or maybe only
from English Google Play, as it apparently was in Arab, only? If we should
re-add it, please create an issue on our GitLab.

> The okex app is a digital currency trading platform

as such, this is probably a custodial offering.

As the website is broken, we can't find any contrary claims and conclude, this
app is **not verifiable**.