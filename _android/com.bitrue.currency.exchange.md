---
title: "Bitrue - Cryptocurrency Wallet & Exchange"
altTitle: 

users: 100000
appId: com.bitrue.currency.exchange
launchDate: 
latestUpdate: 2021-01-13
apkVersionName: "4.3.6"
stars: 3.1
ratings: 1115
reviews: 729
size: 36M
website: https://www.bitrue.com
repository: 
issue: 
icon: com.bitrue.currency.exchange.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BitrueOfficial
providerLinkedIn: 
providerFacebook: BitrueOfficial
providerReddit: 

redirect_from:
  - /com.bitrue.currency.exchange/
---


This app is heavily focused on the "exchange" part which is also in its name.
Nowhere in Google Play can we find claims about self-custody but things like

> - Applies the advanced multi-layer clustered system and the hot/cold wallet
  isolation technology to ensure system security.

only make sense for custodial apps. As a custodial app it is **not verifiable**.