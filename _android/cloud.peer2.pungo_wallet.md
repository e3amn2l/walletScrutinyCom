---
title: "Pungo App"
altTitle: 

users: 1000
appId: cloud.peer2.pungo_wallet
launchDate: 
latestUpdate: 2021-01-20
apkVersionName: "1.26"
stars: 4.0
ratings: 7
reviews: 6
size: 6.2M
website: https://pungowallet.com
repository: 
issue: 
icon: cloud.peer2.pungo_wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-07-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /cloud.peer2.pungo_wallet/
  - /posts/cloud.peer2.pungo_wallet/
---


This app's Google Play description reads:

> Pungo Wallet is focused on ease of use, security, and especially on
  maintaining the end user's financial independence as it’s a noncustodial
  wallet.

That's good. Let's see if this is just a claim ...

Their website `pungowallet.com` forwards to `pungo.io` which does not mention
"wallet" at all.

Their app id [can be found on
GitHub](https://github.com/search?l=XML&q=%22cloud.peer2.pungo_wallet%22&type=Code)
though: [ChainZilla/agamamobilefork](https://github.com/ChainZilla/agamamobilefork).

This repository has only two commits from two years ago, does not claim to be
associated with this app on Google Play and the app was updated on Google Play
recently. With this we conclude the search with the verdict: **not verifiable**.
