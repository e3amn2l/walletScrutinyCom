---
title: "Lead Wallet (BETA) - Store & Swap Cryptocurrencies"
altTitle: 

users: 1000
appId: com.leadwallet.io
launchDate: 
latestUpdate: 2020-12-12
apkVersionName: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: https://www.leadwallet.io
repository: 
issue: 
icon: com.leadwallet.io.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: false
signer: 
reviewArchive:


providerTwitter: Leadwallet
providerLinkedIn: company/leadwallet
providerFacebook: 
providerReddit: LeadWallet

redirect_from:
  - /com.leadwallet.io/
---


The provider makes strong claims about the wallet being non-custodial:

> Lead Wallet is fully decentralized and secure and there is regular auditing
  which ensures that users have total control to send, receive, stake, swap and
  store all sorts of digital assets. In addition, users have full control of
  their private keys which is only stored on a user's device.

The source code though is nowhere to be found. This app is closed source and as
such **not verifiable**.
