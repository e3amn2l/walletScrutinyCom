---
title: "DigiFinex - Buy & Sell Bitcoin, Crypto Trading"
altTitle: 

users: 100000
appId: com.digifinex.app
launchDate: 
latestUpdate: 2021-02-05
apkVersionName: "2021.02.05"
stars: 3.8
ratings: 2190
reviews: 1390
size: 70M
website: https://www.digifinex.com
repository: 
issue: 
icon: com.digifinex.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: DigiFinex
providerLinkedIn: company/digifinex-global
providerFacebook: digifinex.global
providerReddit: DigiFinex

redirect_from:
  - /com.digifinex.app/
---


> DigiFinex is a world’s leading crypto finance exchange

doesn't sound like "wallet" is their primary business and as we can't find any
claims to the contrary, we have to assume this is a custodial offering and thus
**not verifiable**.
