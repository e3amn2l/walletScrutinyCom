---
title: "Bitcoin Wallet - Buy BTC"
altTitle: 

users: 1000000
appId: com.polehin.android
launchDate: 2019-01-01
latestUpdate: 2021-02-04
apkVersionName: "3.4.9"
stars: 4.2
ratings: 14141
reviews: 8179
size: 7.0M
website: https://polehin.com
repository: 
issue: 
icon: com.polehin.android.png
bugbounty: 
verdict: obfuscated # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-30
reviewStale: true
signer: 
reviewArchive:
- date: 2020-03-30
  version: "3.1.4"
  apkHash: 
  gitRevision: 8dc1853115753c1c3ab4e8dc321ee339f071541a
  verdict: custodial
- date: 2019-11-22
  version: "3.1.4"
  apkHash: 
  gitRevision: 90d987f66d51671d7fb7097cd9676bcdce2a7c02
  verdict: nosource

providerTwitter: polehincom
providerLinkedIn: 
providerFacebook: polehincom
providerReddit: 

redirect_from:
  - /bitcoinwallet/
  - /com.polehin.android/
  - /posts/2019/11/bitcoinwallet/
  - /posts/com.polehin.android/
---


The wallet re-branded at some point to Coinbox Bitcoin Wallet and is now a
**blatant scam**.

[As demonstrated by "BTC Sessions" Ben](https://www.youtube.com/watch?v=yP_C4wZyikk)
the wallet features a mnemonic backup that is purely decorative and has nothing
to do with your wallet. Ben mentions a few more red flags that convinced us to
not give them the benefit of the doubt. This is the sort of wallet we try to
warn you of. It is also obfuscating the app, so it gets a bit harder to figure
out what's going on but the evidence as presented in the video leaves no other
conclusion.

**Do not use this app. You will lose your money.**
