---
title: "Cash App"
altTitle: 

users: 10000000
appId: com.squareup.cash
launchDate: 
latestUpdate: 2021-02-04
apkVersionName: "3.32.1"
stars: 4.2
ratings: 318044
reviews: 119654
size: 27M
website: https://cash.app
repository: 
issue: 
icon: com.squareup.cash.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-08-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.squareup.cash/
---


This app is primarily a banking app:

> Cash App is the easiest way to send, spend, save, and invest your money. It’s
  the SAFE, FAST, and FREE mobile banking app.

but you can also receive and send Bitcoins with it:

> **BUY, SELL, DEPOSIT, AND WITHDRAW BITCOIN**
> 
> Cash App is the easiest way to buy, sell, deposit, and withdraw Bitcoin. Track
  the BTC price in realtime in your app and get started by buying as little as
  $1 of Bitcoin. Your BTC arrives in your app instantly. You can then decide to
  keep it safe in Cash App or withdraw it to a different wallet.

To little surprise for a banking app, we can't find any claims about the Bitcoin
Wallet being non-custodial. As a custodial offering, this app is **not
verifiable**.