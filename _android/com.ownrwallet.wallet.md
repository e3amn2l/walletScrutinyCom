---
title: "OWNR Bitcoin wallet and Visa card. Blockchain, BTC"
altTitle: 

users: 50000
appId: com.ownrwallet.wallet
launchDate: 
latestUpdate: 2021-01-22
apkVersionName: "1.3.0"
stars: 4.3
ratings: 728
reviews: 645
size: 66M
website: https://ownrwallet.com
repository: 
issue: 
icon: com.ownrwallet.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ownrwallet
providerLinkedIn: 
providerFacebook: ownrwallet
providerReddit: ownrwallet

redirect_from:
  - /com.ownrwallet.wallet/
---


So this app claims to be a partner of Bitfinex, one of the biggest exchanges:

> - OWNR is an official partner of Bitfinex. 

yet we can't find anywhere that Bitfinex would link to this app. Bitfinex has
[their own app](https://play.google.com/store/apps/details?id=com.bitfinex.mobileapp)
which we will also review shortly.

Bitfinex' Twitter account gets spammed "ownr" **at them** a lot but there is no
mention of that name **by them**. That looks fishy. Maybe they reply to
[this tweet](https://twitter.com/LeoWandersleb/status/1333912501378048002).

In the light of the above, we take the claims:

> - Restore HD-wallets with any seed phrase length (12/15/18/21/24 words)
> 
> - Only you sign the transactions and own your keys – we do no store them on our servers
> 
> Your seed phrase is stored on your device only

with a lot of skepticism and assume this app to be custodial until further
notice from Bitfinex.