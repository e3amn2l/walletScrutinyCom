---
title: "PumaPay Blockchain Wallet 4 Bitcoin & Crypto Coins"
altTitle: 

users: 10000
appId: com.pumapay.pumawallet
launchDate: 
latestUpdate: 2021-02-01
apkVersionName: "3.9.0"
stars: 4.3
ratings: 294
reviews: 196
size: 59M
website: https://pumapay.io
repository: 
issue: 
icon: com.pumapay.pumawallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: PumaPay
providerLinkedIn: company/decentralized-vision
providerFacebook: PumaPayOfficial
providerReddit: 

redirect_from:
  - /com.pumapay.pumawallet/
  - /posts/com.pumapay.pumawallet/
---


This app has very little information on their website but in the Google Play
description we read:

> Please note: It is your sole responsibility to keep the 12-word seed phrase of
  the wallet in a safe place for you to be able to access it in the future. In
  this case, you will be required to restore the Private Key.

So this sounds like they claim to be non-custodial but we cannot see any source
code which makes the app **not verifiable**.
