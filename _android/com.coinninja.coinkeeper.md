---
title: "DropBit: Bitcoin Wallet"
altTitle: 

users: 10000
appId: com.coinninja.coinkeeper
launchDate: 2018-08-01
latestUpdate: 2020-01-28
apkVersionName: "3.2.7"
stars: 2.8
ratings: 165
reviews: 120
size: 33M
website: https://dropbit.app
repository: https://github.com/coinninjadev/dropbit-android
issue: https://github.com/coinninjadev/dropbit-android/issues/1
icon: com.coinninja.coinkeeper.png
bugbounty: 
verdict: defunct # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-04-08
reviewStale: false
signer: 
reviewArchive:
- date: 2019-11-24
  version: "3.2.7"
  apkHash: 
  gitRevision: a920a50eb4b0f8638e7cedb013a135f9c0a7b0fc
  verdict: nonverifiable

providerTwitter: dropbitapp
providerLinkedIn: 
providerFacebook: DropBit-2094204254174419
providerReddit: DropBit

redirect_from:
  - /bropbit/
  - /dropbit/
  - /com.coinninja.coinkeeper/
  - /posts/2019/11/bropbit/
  - /posts/2019/11/dropbit/
  - /posts/com.coinninja.coinkeeper/
---


Dropbit was a custodial lightning wallet and appears to still be a non-custodial
on-chain bitcoin wallet. As all funds on the lightning side are locked up with
no comment from the provider
([who apparently is in jail](https://www.reddit.com/r/DropBit/comments/fmgoad/sats_stuck_in_dropbit_lightning_side/)),
we mark this wallet as defunct for now.