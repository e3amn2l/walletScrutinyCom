---
title: "Bitrefill - Use Bitcoin to buy Gift Cards & Topups"
altTitle: 

users: 50000
appId: com.bitrefill.app
launchDate: 2018-04-10
latestUpdate: 2019-09-28
apkVersionName: "1.28.9"
stars: 4.2
ratings: 410
reviews: 240
size: 5.2M
website: https://www.bitrefill.com
repository: 
issue: 
icon: com.bitrefill.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-25
reviewStale: false
signer: 
reviewArchive:


providerTwitter: bitrefill
providerLinkedIn: 
providerFacebook: bitrefill
providerReddit: Bitrefill

redirect_from:
  - /com.bitrefill.app/
  - /posts/com.bitrefill.app/
---


While the primary purpose of this app is to buy stuff with Bitcoin and it appears
to be possible to use the app without putting money into it, the app also can
hold a balance, so it appears to be a wallet. At least we take that from the
screenshots.

As the description has no claims to the contrary and we can't find anything about
the app on their website except for a link to the Playstore, we have to assume
it is a custodial service.

Our verdict: The app is **not verifiable**.
