---
title: "Bitnovo - Crypto Wallet"
altTitle: 

users: 10000
appId: com.bitnovo.app
launchDate: 
latestUpdate: 2020-12-02
apkVersionName: "2.8.3"
stars: 2.7
ratings: 244
reviews: 174
size: 34M
website: http://www.bitnovo.com
repository: 
issue: 
icon: com.bitnovo.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-06
reviewStale: false
signer: 
reviewArchive:


providerTwitter: bitnovo
providerLinkedIn: 
providerFacebook: BitcoinBitnovo
providerReddit: 

redirect_from:
  - /com.bitnovo.app/
---


The reviews don't let us expect this wallet to be around for long. 2.7* with
tons of scam accusations.

The provider clearly controls the funds and thus this "wallet" is **not
verifiable**.

