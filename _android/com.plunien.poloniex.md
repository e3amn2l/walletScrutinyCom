---
title: "Poloniex Crypto Exchange"
altTitle: 

users: 100000
appId: com.plunien.poloniex
launchDate: 
latestUpdate: 2021-02-01
apkVersionName: "1.23.7"
stars: 4.4
ratings: 3041
reviews: 1641
size: 37M
website: https://support.poloniex.com
repository: 
issue: 
icon: com.plunien.poloniex.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Poloniex
providerLinkedIn: 
providerFacebook: poloniex
providerReddit: 

redirect_from:
  - /com.plunien.poloniex/
  - /posts/com.plunien.poloniex/
---


This app is not primarily advertised as a wallet. It is an interface to a crypto
exchange but on the Google Play description we read:

> Manage your balance and trades on the go so you never miss a market move.
  Deposit and withdraw from your crypto wallet, monitor account balances and
  orders, view real-time ticker updates across all markets, and choose your
  favorite cryptocurrencies and create price alerts for them.

So it has a wallet integrated which is custodial by the sound of it and
therefore **not verifiable**.