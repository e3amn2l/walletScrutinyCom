---
title: "Bitcoin Wallet & Ethereum Ripple ZIL DOT"
altTitle: 

users: 500000
appId: io.atomicwallet
launchDate: 2019-01-30
latestUpdate: 2021-01-19
apkVersionName: "0.71.0"
stars: 4.3
ratings: 20583
reviews: 10934
size: 13M
website: https://atomicwallet.io
repository: 
issue: 
icon: io.atomicwallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: atomicwallet
providerLinkedIn: 
providerFacebook: atomicwallet
providerReddit: 

redirect_from:

---


Bitcoin Wallet & Ethereum Ripple Tron EOS
is a non-custodial wallet according to their description:

> Atomic Wallet is universal non-custodial app for over 300 cryptocurrencies.

Unfortunately they do not share all sources for the Android app.

Verdict: This wallet is **not verifiable**.
