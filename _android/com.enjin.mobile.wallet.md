---
title: "Enjin: Bitcoin, Ethereum, Blockchain Crypto Wallet"
altTitle: 

users: 500000
appId: com.enjin.mobile.wallet
launchDate: 2018-01-01
latestUpdate: 2021-01-25
apkVersionName: "1.11.1-r"
stars: 4.5
ratings: 6766
reviews: 4065
size: 31M
website: https://enjin.io/products/wallet
repository: 
issue: 
icon: com.enjin.mobile.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: enjin
providerLinkedIn: company/enjin
providerFacebook: enjinsocial
providerReddit: EnjinCoin

redirect_from:
  - /enjin/
  - /com.enjin.mobile.wallet/
  - /posts/2019/11/enjin/
  - /posts/com.enjin.mobile.wallet/
---


Enjin: Blockchain & Crypto Wallet
description starts promising:

> "Your private keys are your own"

They advertise advanced securing techniques among which are:

> "An extensive independent security audit and penetration test found no
> security issues."

(You can read the report [here](https://cdn.enjin.io/files/pdfs/enjin-wallet-security-audit.pdf))

but source code isn't available on [their website](https://github.com/enjin).
So the user is left with only one choice: trust.

Our verdict: **not verifiable**.


Other observations
------------------

> in-app browsing:
> "ENJOY SEAMLESS BROWSING
> Interact with any DApp with the single click of a button—without leaving the
> safety of your crypto wallet."

looks very advanced, the list of features is tremendous. also an old player:
"ABOUT ENJIN
Founded in 2009 and based in Singapore, Enjin offers an ecosystem of integrated, user-first blockchain products that enable anyone to easily manage, explore, distribute, and integrate blockchain-based assets."

on their main page, they advertise advanced securing techniques amongst which are:
- "An extensive independent security audit and penetration test found no security issues." (with ability to read the [report](https://cdn.enjin.io/files/pdfs/enjin-wallet-security-audit.pdf))
- "Custom ARM instructions ensure that sensitive data is instantly deleted from your phone's memory."
- "Enjin Keyboard. Built from scratch to protect you from any form of data sniffing or keyloggers."
