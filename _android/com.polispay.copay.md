---
title: "PolisPay - Cryptocurrency wallet"
altTitle: 

users: 5000
appId: com.polispay.copay
launchDate: 2018-02-21
latestUpdate: 2020-12-24
apkVersionName: "8.8.0"
stars: 4.3
ratings: 121
reviews: 70
size: 10M
website: https://www.polispay.com
repository: 
issue: 
icon: com.polispay.copay.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.polispay.copay/
  - /posts/com.polispay.copay/
---


This app appears to be a [CoPay](/copay/) clone given its app ID:
`com.polispay.copay`.

In the app's description we read:

> A unique wallet based on mnemonic phrases and public and private extended keys
> PolisPay wallet is impossible to hack.

which sounds like a claim to be non-custodial. Let's see if there is source code
somewhere ... nope. No mention of its source code or a repository.

This app is **not verifiable**.

