---
title: "VisionWallet — Crypto & Bitcoin Wallet"
altTitle: 

users: 1000
appId: com.visionwallet.app
launchDate: 
latestUpdate: 2020-11-05
apkVersionName: "1.3.2"
stars: 3.9
ratings: 74
reviews: 69
size: 4.9M
website: https://visionwallet.com/en
repository: 
issue: 
icon: com.visionwallet.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.visionwallet.app/
---


> Passive funds accumulation (interest on the account balance)

A feature like this makes no sense in a non-custodial app and as there are no
actual claims about this app being non-custodial, we file it as custodial and
thus **not verifiable**.
