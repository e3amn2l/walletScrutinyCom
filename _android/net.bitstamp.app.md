---
title: "Bitstamp – Buy & Sell Bitcoin at Crypto Exchange"
altTitle: 

users: 100000
appId: net.bitstamp.app
launchDate: 
latestUpdate: 2021-01-11
apkVersionName: "1.6.3"
stars: 4.4
ratings: 8540
reviews: 2351
size: 13M
website: https://www.bitstamp.net
repository: 
issue: 
icon: net.bitstamp.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Bitstamp
providerLinkedIn: company/bitstamp
providerFacebook: Bitstamp
providerReddit: 

redirect_from:

---


On the Google Play description we read:

> Convenient, but secure
>
> ● We store 98% of all crypto assets in cold storage

which means you don't get the keys for your coins. This is a custodial service
and therefore **not verifiable**.