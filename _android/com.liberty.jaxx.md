---
title: "Jaxx Liberty: Blockchain Wallet"
altTitle: 

users: 100000
appId: com.liberty.jaxx
launchDate: 2018-09-01
latestUpdate: 2021-02-01
apkVersionName: "2.5.0"
stars: 3.7
ratings: 4213
reviews: 2344
size: 17M
website: https://jaxx.io
repository: 
issue: 
icon: com.liberty.jaxx.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-11-02
reviewStale: true
signer: 
reviewArchive:


providerTwitter: jaxx_io
providerLinkedIn: 
providerFacebook: JaxxWallet
providerReddit: 

redirect_from:
  - /jaxx/
  - /com.liberty.jaxx/
  - /posts/2019/11/jaxx/
  - /posts/com.liberty.jaxx/
---


Jaxx Liberty: Blockchain Wallet
is non-custodial:

> "Get started with an industry-standard 12-word backup phrase that is portable to and from other wallets. Sensitive information like your backup phrase and private keys are never stored anywhere but your device. Jaxx Liberty is a non-custodial wallet, which means you own the keys. You're in control. We can’t even access them."

The maker of Jaxx Classic & Jaxx Liberty is [Decentral](https://decentral.ca/)
which was founded by Anthony Di Iorio, co-founder of Ethereum in 2013.

We couldn't find source code for an Android wallet. Looking for the code we came
across this article:
["Jaxx Liberty Wallet Review: Complete Beginners Guide"](https://www.coinbureau.com/review/jaxx-liberty-wallet/). Some excerpts:

> "the old Jaxx Classic wallet in June 2017 led to the loss of $400,000 in various cryptocurrencies from user wallets. Most of that loss was from a single user's wallet, and in that case, it could have been avoided since the user was managing his Jaxx on a rooted Android device."

> "There is also an improved new security model that protects your sensitive information with a strong password, using AES-256 encryption enhanced by 5000 rounds of PBKDF2 password hashing."

> "Two things that are missing are 2-factor authentication and open source code. That said, they do make all the code viewable, with the exception of the code for the UI."

That last one we were looking for. Without the full source code and full
instructions on how to build the wallet, a reproducible build is not possible.

Verdict: This wallet is **not verifiable**.
