---
title: "Paytomat Wallet: Bitcoin, Ethereum, EOS, tokens"
altTitle: 

users: 10000
appId: com.paytomat
launchDate: 2018-06-20
latestUpdate: 2021-02-01
apkVersionName: "1.37.2"
stars: 4.1
ratings: 746
reviews: 390
size: 6.4M
website: http://www.paytomat.com
repository: 
issue: 
icon: com.paytomat.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-25
reviewStale: true
signer: 
reviewArchive:


providerTwitter: paytomat
providerLinkedIn: company/11433285
providerFacebook: paytomat
providerReddit: 

redirect_from:
  - /com.paytomat/
  - /posts/com.paytomat/
---


This app gets straight to the point:

> The wallet is non-custodial, meaning the private keys never leave the secure
storage of your phone.

but can we verify this?

On their website we find a [link to their GitHub](https://github.com/Paytomat)
but none of

* **wallet-connect-android** Binance implementation of Wallet Connect protocol as kotlin library
* **ScatterKit** ScatterKit allows communication between Swift applications and web pages that use Scatter plugin
* **Paytomat-iOS-SDK** Send transactions and get account information via Paytomat Wallet
* **Paytomat-Android-SDK** Send transactions and get account information via Paytomat Wallet
* **paytomat-api** Docs for various Paytomat APIs

sounds like an actual Wallet.

Searching GitHub for
[their app id](https://github.com/search?q=%22com.paytomat%22&type=Code) yields
401 results which we should not be tasked to look through one by one.

So for now we assume it is closed source and come to the verdict: **not verifiable**.
