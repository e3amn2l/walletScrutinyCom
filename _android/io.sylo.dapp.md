---
title: "Sylo - Smart Wallet & Messenger"
altTitle: 

users: 100000
appId: io.sylo.dapp
launchDate: 
latestUpdate: 2020-12-21
apkVersionName: "3.0.6-1"
stars: 4.3
ratings: 549
reviews: 283
size: 336M
website: https://www.sylo.io/wallet
repository: 
issue: 
icon: io.sylo.dapp.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-06-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: sylo
providerLinkedIn: company/sylo.io
providerFacebook: sylo.io
providerReddit: sylo_io

redirect_from:
  - /io.sylo.dapp/
  - /posts/io.sylo.dapp/
---


This app recently came to our attention as it "can be used to buy coke with
bitcoin". Their Google Play description indeed lists BTC as one of the supported
currencies and

> Only you have the key - it's on your terms.

is clearly a claim of not being custodial. So ... where is the source code to
verify the claims?

Turns out we cannot find any source code for this wallet. As a closed source app
it is **not verifiable**.
