---
title: "arzjoo"
altTitle: 

users: 10000
appId: com.arzjoo
launchDate: 
latestUpdate: 2021-01-30
apkVersionName: "1.3.5"
stars: 3.2
ratings: 1165
reviews: 472
size: Varies with device
website: https://arzjoo.com
repository: 
issue: 
icon: com.arzjoo.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: arzjoo
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.arzjoo/
---


This provider is from Iran and its website is in Farsi, only. Google Translate
[helps to read it in English](https://translate.google.com/translate?hl=en&sl=auto&tl=en&u=https%3A%2F%2Farzjoo.com%2F)
and according to the translation, the only thing they say about the security of
the wallet is that it's super secure:

> high security
> 
> The security of this wallet is guaranteed by Arzjoo and you can trust it to
  keep the currency password.

As there are no claims to the contrary, we assume this is a custodial offering
and thus **not verifiable**.
