---
title: "Shakepay: Buy Bitcoin in Canada"
altTitle: 

users: 100000
appId: com.shaketh
launchDate: 
latestUpdate: 2021-01-28
apkVersionName: "1.6.55"
stars: 4.2
ratings: 2757
reviews: 1463
size: 75M
website: https://shakepay.com
repository: 
issue: 
icon: com.shaketh.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: shakepay
providerLinkedIn: 
providerFacebook: shakepay
providerReddit: shakepay

redirect_from:
  - /com.shaketh/
  - /posts/com.shaketh/
---


This app claims to be a wallet:

> Canadians buy and sell digital currencies in minutes with the Shakepay wallet.

In their [FAQ](https://help.shakepay.com/en/articles/1721007-what-happens-if-i-lose-my-phone)
they explain what you have to do if you lose your phone, once you have a new
phone:

> We’ll be able to update your Shakepay account's phone number and reset your
  2-Factor Authentication code.

That's all. If that's all, they must have the private keys which makes this app
a custodial service and thus **not verifiable**.