---
title: "Busha: Buy & Sell Bitcoin, Ethereum. Crypto Wallet"
altTitle: 

users: 50000
appId: co.busha.android
launchDate: 2019-01-21
latestUpdate: 2021-01-06
apkVersionName: "2.6.4"
stars: 4.2
ratings: 1851
reviews: 1443
size: 16M
website: https://busha.co
repository: 
issue: 
icon: co.busha.android.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: getbusha
providerLinkedIn: 
providerFacebook: getbusha
providerReddit: 

redirect_from:
  - /co.busha.android/
  - /posts/co.busha.android/
---


The description

> Won’t you rather trade and store your crypto assets on a platform you can
  trust? Busha is a Nigerian based crypto exchange that offers you all these and
  more.

sounds like it's an app to access an account on a custodial platform.

And there is nothing on the website indicating otherwise.

Our verdict: **not verifiable**.