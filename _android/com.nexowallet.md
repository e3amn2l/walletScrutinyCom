---
title: "Nexo - Crypto Banking Account"
altTitle: 

users: 500000
appId: com.nexowallet
launchDate: 
latestUpdate: 2021-01-22
apkVersionName: "1.3.1"
stars: 4.1
ratings: 5933
reviews: 2763
size: 50M
website: https://nexo.io
repository: 
issue: 
icon: com.nexowallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer: 
reviewArchive:


providerTwitter: NexoFinance
providerLinkedIn: 
providerFacebook: nexofinance
providerReddit: Nexo

redirect_from:
  - /com.nexowallet/
---


In the description on Google Play we read:

> • 100% Secured by Leading Audited Custodian BitGo

which makes it a custodial app. The custodian is claimed to be "BitGo" so as a
user you have to trust BitGo to not lose the coins and Nexo to actually not hold
all or part of the coins. In any case this app is **not verifiable**.