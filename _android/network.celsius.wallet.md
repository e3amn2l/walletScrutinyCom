---
title: "Celsius - Crypto Wallet"
altTitle: 

users: 100000
appId: network.celsius.wallet
launchDate: 
latestUpdate: 2021-01-20
apkVersionName: "4.7.0"
stars: 3.8
ratings: 3755
reviews: 2853
size: 85M
website: https://celsius.network
repository: 
issue: 
icon: network.celsius.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: celsiusnetwork
providerLinkedIn: company/celsiusnetwork
providerFacebook: CelsiusNetwork
providerReddit: 

redirect_from:
  - /network.celsius.wallet/
---


> Use our fully functioning & secure crypto wallet & crypto lending platform to
  transfer and withdraw your Ethereum, Bitcoin, and over 30 other
  cryptocurrencies, free.

sounds like also a Bitcoin wallet.

The focus on "lending platform" doesn't make us hope for non-custodial parts to
it though ...

And sure enough, nowhere on the website can we find about this app being
non-custodial. As a custodial app, it is **not verifiable**.
