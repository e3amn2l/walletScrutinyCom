---
title: "BitShield Wallet - Bitcoin Wallet"
altTitle: 

users: 10000
appId: com.bitshield.bitshieldwallet
launchDate: 2020-06-09
latestUpdate: 2020-06-19
apkVersionName: "1.27"
stars: 4.6
ratings: 1183
reviews: 700
size: 57M
website: https://bitshieldwallet.com
repository: 
issue: 
icon: com.bitshield.bitshieldwallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.bitshield.bitshieldwallet/
---


In this app's description we read:

> - Non-Custodial. You own your wallet private keys.

A 5.0 stars rating from 957 ratings doesn't look natural but let's see if it's
open source.

On their website:

> We are privacy activists who have dedicated our lives to creating the software
  that Silicon Valley will never build. We build the software that Bitcoin
  deserves.

which was taken almost word for word from
[another wallet](/com.samourai.wallet/)'s website.

... which leads us to wonder if the provider is also secretive about who they
are and sure enough, no mention of the people behind this product.

The domain name owner is not on public record neither:

> Registrant Name: WhoisGuard Protected
> 
> Registrant Organization: WhoisGuard, Inc.
> 
> ...

We have no problem with privacy minded providers as long as the product can
be fully and easily verified. In this case we do not even find a claim of
public source and the Xamarin based app contains native code, making it hard to
get any insights. Anyway, by our standards it is **not verifiable** at all.
