---
title: "Coinbase Pro – Bitcoin & Crypto Trading"
altTitle: 

users: 100000
appId: com.coinbase.pro
launchDate: 
latestUpdate: 2021-02-02
apkVersionName: "1.0.62"
stars: 4.3
ratings: 3567
reviews: 2178
size: 35M
website: https://pro.coinbase.com
repository: 
issue: 
icon: com.coinbase.pro.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinbasePro
providerLinkedIn: 
providerFacebook: coinbase
providerReddit: 

redirect_from:
  - /com.coinbase.pro/
  - /posts/com.coinbase.pro/
---


This is the interface for a trading platform aka exchange. The funds are stored
with the provider. As a custodial service it is **not verifiable**.