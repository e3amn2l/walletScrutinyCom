---
title: "Electron Cash wallet for Bitcoin Cash"
altTitle: 

users: 10000
appId: org.electroncash.wallet
launchDate: 2018-12-08
latestUpdate: 2020-03-29
apkVersionName: "4.0.14-0"
stars: 4.5
ratings: 69
reviews: 35
size: 32M
website: https://electroncash.org
repository: 
issue: 
icon: org.electroncash.wallet.png
bugbounty: 
verdict: nobtc # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /org.electroncash.wallet/
  - /posts/org.electroncash.wallet/
---


