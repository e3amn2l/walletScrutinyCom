---
title: "Luno: Buy Bitcoin, Ethereum and Cryptocurrency"
altTitle: 

users: 5000000
appId: co.bitx.android.wallet
launchDate: 2014-11-01
latestUpdate: 2021-01-29
apkVersionName: "7.6.0"
stars: 4.2
ratings: 61962
reviews: 34863
size: 18M
website: https://www.luno.com
repository: 
issue: 
icon: co.bitx.android.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-12
reviewStale: true
signer: 
reviewArchive:
- date: 2019-11-14
  version: "6.8.0"
  apkHash: 
  gitRevision: 372c9c03c6422faed457f1a9975d7cab8f13d01f
  verdict: nosource

providerTwitter: LunoGlobal
providerLinkedIn: company/lunoglobal
providerFacebook: luno
providerReddit: 

redirect_from:
  - /luno/
  - /co.bitx.android.wallet/
  - /posts/2019/11/luno/
  - /posts/co.bitx.android.wallet/
---


Luno: Buy Bitcoin, Ethereum and Cryptocurrency
advertises on the Playstore:

> **Keeping your crypto safe**<br>
> The majority of customer Bitcoin funds are kept in what we call “deep freeze” storage. These are multi-signature wallets, with private keys stored in different bank vaults. No single person ever has access to more than one key. We maintain a multi-signature hot wallet to facilitate instant Bitcoin withdrawals.

This tells us we are dealing with a custodial wallet here. Our verdict: **not
verifiable**.
