---
title: "HandCash - Bitcoin SV Wallet (BETA)"
altTitle: 

users: 10000
appId: com.freaks.app.handcash
launchDate: 2018-06-01
latestUpdate: 2019-09-16
apkVersionName: "1.5.17"
stars: 4.4
ratings: 344
reviews: 205
size: 28M
website: https://handcash.io
repository: 
issue: 
icon: com.freaks.app.handcash.png
bugbounty: 
verdict: defunct # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-25
reviewStale: false
signer: 
reviewArchive:


providerTwitter: handcashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.freaks.app.handcash/
  - /posts/com.freaks.app.handcash/
---


This was the beta version of [this BSV wallet](/io.handcash.wallet/).