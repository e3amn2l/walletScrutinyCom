---
title: "Bitso - Buy and sell bitcoin"
altTitle: 

users: 1000000
appId: com.bitso.wallet
launchDate: 
latestUpdate: 2021-02-03
apkVersionName: "2.19.2"
stars: 3.2
ratings: 6835
reviews: 4018
size: 28M
website: https://bitso.com/app
repository: 
issue: 
icon: com.bitso.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Bitso
providerLinkedIn: 
providerFacebook: bitsoex
providerReddit: 

redirect_from:
  - /com.bitso.wallet/
---


Bitso appears to be an exchange and as so often, we see no mentions of security
in the app description or the website and have to assume it is a custodial
offering and thus **not verifiable**.
