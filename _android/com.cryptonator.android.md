---
title: "Cryptonator cryptocurrency wallet"
altTitle: 

users: 100000
appId: com.cryptonator.android
launchDate: 2018-11-01
latestUpdate: 2021-01-22
apkVersionName: "4.0"
stars: 3.8
ratings: 4657
reviews: 2701
size: 8.7M
website: https://www.cryptonator.com
repository: 
issue: 
icon: com.cryptonator.android.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-03-17
reviewStale: true
signer: 
reviewArchive:
- date: 2019-11-12
  version: "3.0.1"
  apkHash: 
  gitRevision: acb5634ce0405f12d9924759b045407fde297306
  verdict: nosource

providerTwitter: cryptonatorcom
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /cryptonator/
  - /com.cryptonator.android/
  - /posts/2019/11/cryptonator/
  - /posts/com.cryptonator.android/
---


Cryptonator cryptocurrency wallet
makes no claim to be non-custodial but the
[Customer Support](https://www.cryptonator.com/contact/other/)
is pretty unambigously pointing towards it being custodial:

> **Do you provide private keys?**: No

Absent source code this wallet is **not verifiable**.
