---
title: "Crypto Wallet 2020"
altTitle: 

users: 5000
appId: com.thinkdevs.cryptomarket
launchDate: 
latestUpdate: 2020-11-13
apkVersionName: "0.0.9"
stars: 3.8
ratings: 48
reviews: 32
size: 3.0M
website: 
repository: 
issue: 
icon: com.thinkdevs.cryptomarket.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.thinkdevs.cryptomarket/
---


This is not a wallet:

> Crypto Wallet gives you quick and easy access to cryptocurrency prices, details.