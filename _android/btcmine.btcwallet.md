---
title: "BTC Wallet"
altTitle: 

users: 1000
appId: btcmine.btcwallet
launchDate: 
latestUpdate: 2017-08-13
apkVersionName: "1.1"
stars: 3.6
ratings: 13
reviews: 7
size: 2.3M
website: 
repository: 
issue: 
icon: btcmine.btcwallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /btcmine.btcwallet/
---


This app features no website or claims about the custody of coins. Its
description starts with:

> Download share and win. If your friends download app and send btc to their own
  wallet, you and your friends will win 0,001 BTC!!

and continues with more not so meaningful words. This app is probably a scam and
absent claims of being non-custodial we file it as **not verifiable**.
