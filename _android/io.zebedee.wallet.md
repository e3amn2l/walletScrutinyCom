---
title: "ZEBEDEE Wallet"
altTitle: 

users: 500
appId: io.zebedee.wallet
launchDate: 
latestUpdate: 2021-02-01
apkVersionName: "11.0.0"
stars: 5.0
ratings: 7
reviews: 4
size: 59M
website: 
repository: 
issue: 
icon: io.zebedee.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.zebedee.wallet/
---


