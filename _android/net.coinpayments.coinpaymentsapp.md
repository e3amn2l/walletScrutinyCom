---
title: "CoinPayments - Crypto Wallet for Bitcoin/Altcoins"
altTitle: 

users: 100000
appId: net.coinpayments.coinpaymentsapp
launchDate: 2016-11-15
latestUpdate: 2021-01-22
apkVersionName: "Varies with device"
stars: 3.5
ratings: 1832
reviews: 952
size: Varies with device
website: https://www.coinpayments.net
repository: 
issue: 
icon: net.coinpayments.coinpaymentsapp.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinPaymentsNET
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /net.coinpayments.coinpaymentsapp/
  - /posts/net.coinpayments.coinpaymentsapp/
---


The description is not very clear but sounds a bit like this app is custodial.

Their website is mainly about payment integrations for merchants but also has a
[page dedicated to the mobile apps](https://www.coinpayments.net/apps) which is
not very detailed but our guess for now is that

> Now you can easily access your
CoinPayments account to send and receive coins, accept POS payments in person,
and exchange many of our coins anywhere you have internet access.

means you can access your coins which are stored on their servers thus this is a
custodial app.

Our verdict: **not verifiable**.
