---
title: "Kraken Futures: Bitcoin & Crypto Futures Trading"
altTitle: 

users: 10000
appId: com.krakenfutures
launchDate: 
latestUpdate: 2020-12-04
apkVersionName: "5.23.2"
stars: 3.5
ratings: 71
reviews: 29
size: 12M
website: https://futures.kraken.com
repository: 
issue: 
icon: com.krakenfutures.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.krakenfutures/
  - /posts/com.krakenfutures/
---


This is the interface for an exchange and nothing in the description hints at
non-custodial parts to it.
