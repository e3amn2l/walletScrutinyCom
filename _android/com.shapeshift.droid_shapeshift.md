---
title: "ShapeShift Buy & Trade Bitcoin & Top Crypto Assets"
altTitle: 

users: 100000
appId: com.shapeshift.droid_shapeshift
launchDate: 
latestUpdate: 2021-01-27
apkVersionName: "2.10.0"
stars: 2.2
ratings: 2214
reviews: 1414
size: 52M
website: https://shapeshift.com
repository: 
issue: 
icon: com.shapeshift.droid_shapeshift.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ShapeShift_io
providerLinkedIn: 
providerFacebook: ShapeShiftPlatform
providerReddit: 

redirect_from:
  - /com.shapeshift.droid_shapeshift/
---


ShapeShift is best known for their non-custodial exchange but this app appears
to be a wallet:

> STORE YOUR CRYPTO IN A SECURE WALLET
> Setup a ShapeShift multi-chain wallet in seconds to store your crypto.

... and non-custodial:

> ShapeShift makes self-custody easy, never holding your coins, so you have
  complete control over your assets.

but is their code public? On Google Play we see no such claim.

On the [referenced website](https://shapeshift.com/) there is no link back to
the app on Android.

And clicking around on their website the app is nowhere to be found ... unless
... the page behind [Learn -> Bitcoin](https://shapeshift.com/category/bitcoin)
is what we are looking for ... oh, no, *Introducing: The ShapeShift Mobile App*
is only a link to [this page](https://shapeshift.com/library/mobile-app) which
again has no link to Google Play? Only a link to
[this invite page](https://shapeshift.com/invite) which asks for your email
address to get the app? Seriously? Here we give up our search for source code or
more information on their website. As on GitHub we
[can't find the appId](https://github.com/search?q=%22com.shapeshift.droid_shapeshift%22&type=code)
neither, we conclude the app is closed source and thus **not verifiable**.