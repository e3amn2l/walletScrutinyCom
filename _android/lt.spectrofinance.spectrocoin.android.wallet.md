---
title: "Bitcoin Wallet by SpectroCoin"
altTitle: 

users: 500000
appId: lt.spectrofinance.spectrocoin.android.wallet
launchDate: 2014-12-05
latestUpdate: 2021-02-05
apkVersionName: "1.15.3"
stars: 3.1
ratings: 3050
reviews: 870
size: 12M
website: https://spectrocoin.com
repository: 
issue: 
icon: lt.spectrofinance.spectrocoin.android.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: spectrocoin
providerLinkedIn: company/spectrocoin
providerFacebook: spectrocoin
providerReddit: 

redirect_from:
  - /lt.spectrofinance.spectrocoin.android.wallet/
  - /posts/lt.spectrofinance.spectrocoin.android.wallet/
---


This part of the wallet's descriptions certainly sounds like a custodial app:

> We respect your privacy and collect only necessary data, which is processed and stored in accordance with applicable laws. We give you great control, so you can always manage and update your SpectroCoin account easily. As with so many things, it is a matter of balancing security and convenience. A safe environment, in which you can exchange cryptocurrency and make borderless payments, requires industry-leading safety standards, including multi-stage account verification, advanced authentication methods, and bank-level security.

Also the user comments are a lesson in why a custodial wallet is not for everyone.

Their website has the confirmation about the app being custodial:

> 99% of all digital currency is stored in protected SpectroCoin offline storage
(deep cold wallet) to secure the assets

Our verdict: **not verifiable**
