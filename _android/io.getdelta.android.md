---
title: "Delta - Bitcoin & Cryptocurrency Portfolio Tracker"
altTitle: 

users: 500000
appId: io.getdelta.android
launchDate: 
latestUpdate: 2020-11-18
apkVersionName: "3.6.1"
stars: 4.5
ratings: 17258
reviews: 6577
size: 59M
website: 
repository: 
issue: 
icon: io.getdelta.android.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.getdelta.android/
---


This appears to be only a portfolio tracker. If it asks for your credentials for
exchanges, it might still get into a position of pulling your funds from there.