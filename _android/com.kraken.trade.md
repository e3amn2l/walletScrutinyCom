---
title: "Kraken Pro: Advanced Bitcoin & Crypto Trading"
altTitle: 

users: 100000
appId: com.kraken.trade
launchDate: 
latestUpdate: 2021-02-02
apkVersionName: "1.5.11-5438"
stars: 4.2
ratings: 9373
reviews: 3365
size: 45M
website: https://www.kraken.com
repository: 
issue: 
icon: com.kraken.trade.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: krakenfx
providerLinkedIn: company/krakenfx
providerFacebook: KrakenFX
providerReddit: 

redirect_from:
  - /com.kraken.trade/
  - /posts/com.kraken.trade/
---


On their website we read:

> 95% of all deposits are kept in offline, air-gapped, geographically
  distributed cold storage. We keep full reserves so that you can always
  withdraw immediately on demand.

This app is an interface to a custodial exchange and therefore **not
verifiable**.