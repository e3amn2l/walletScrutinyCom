---
title: "TenX - Buy Bitcoin & Crypto Card"
altTitle: 

users: 100000
appId: com.onebit.app
launchDate: 2017-11-23
latestUpdate: 2020-11-11
apkVersionName: "3.25.1"
stars: 3.6
ratings: 1038
reviews: 551
size: 82M
website: https://www.tenx.tech
repository: 
issue: 
icon: com.onebit.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.onebit.app/
  - /posts/com.onebit.app/
---


This app's description hints at being custodial with

> Send cryptocurrencies to other TenX Wallet users for free

as there is no transaction fee if the transaction is just an update of their
centralized database.

As the core product of TenX is to spend your Bitcoins via a credit card, they
probably want to be in control of the coins and not surrender this control to
the user. We assume it is custodial.

Our verdict: **not verifiable**.
