---
title: "eToro - Smart crypto trading made easy"
altTitle: 

users: 5000000
appId: com.etoro.openbook
launchDate: 2013-11-05
latestUpdate: 2021-01-12
apkVersionName: "296.0.0"
stars: 1.8
ratings: 63117
reviews: 24427
size: 43M
website: 
repository: 
issue: 
icon: com.etoro.openbook.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: etoro
providerLinkedIn: company/etoro
providerFacebook: eToro
providerReddit: 

redirect_from:
  - /com.etoro.openbook/
  - /posts/com.etoro.openbook/
---


This page was created by a script from the **appId** "com.etoro.openbook" and public
information found
[here](https://play.google.com/store/apps/details?id=com.etoro.openbook).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.