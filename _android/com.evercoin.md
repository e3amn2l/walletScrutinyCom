---
title: "Evercoin: Bitcoin, Ripple, ETH"
altTitle: 

users: 10000
appId: com.evercoin
launchDate: 
latestUpdate: 2020-11-22
apkVersionName: "2.8.8"
stars: 3.7
ratings: 184
reviews: 130
size: 40M
website: https://evercoin.com
repository: 
issue: 
icon: com.evercoin.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: everc0in
providerLinkedIn: 
providerFacebook: evercoin
providerReddit: 

redirect_from:
  - /com.evercoin/
  - /posts/com.evercoin/
---


This app's description says:

> Evercoin is an integrated non-custodial wallet for managing and exchanging
  cryptocurrencies.

So ... is there source coude to reproduce the build?

Unfortunately there is no mention of source code anywhere. Absent source code
this app is **not verifiable**.
