---
title: "Dompet Bitcoin Indonesia"
altTitle: 

users: 100000
appId: co.id.dompet.btc.indonesia
launchDate: 
latestUpdate: 2020-12-08
apkVersionName: "Varies with device"
stars: 3.5
ratings: 4505
reviews: 2581
size: Varies with device
website: https://www.indodax.com
repository: 
issue: 
icon: co.id.dompet.btc.indonesia.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.id.dompet.btc.indonesia/
---


This app looks like a terminal for a website:

> Dompet Bitcoin Indonesia is a bitcoin wallet that is fully integrated with
  Indodax.com. You can now easily access every feature that is available in
  Indodax.com website through your smartphones only with a few clicks of your
  fingers.

and that website is an exchange(?). We assume that like in many such cases the
app is a custodial offering and thus **not verifiable**.
