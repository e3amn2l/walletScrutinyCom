---
title: "Unstoppable - Invest In Crypto"
altTitle: 

users: 5000
appId: io.horizontalsystems.bankwallet
launchDate: 2018-12-18
latestUpdate: 2021-01-11
apkVersionName: "0.18.2"
stars: 4.3
ratings: 312
reviews: 271
size: 42M
website: https://unstoppable.money
repository: https://github.com/horizontalsystems/unstoppable-wallet-android
issue: 
icon: io.horizontalsystems.bankwallet.png
bugbounty: 
verdict: reproducible # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-19
reviewStale: false
signer: c1899493e440489178b8748851b72cbed50c282aaa8c03ae236a4652f8c4f27b
reviewArchive:
- date: 2020-12-29
  version: "0.18.1"
  apkHash: be565a137aa46f4777ab4d7ee56045a531fdc55de5826104d73bd413852709e3
  gitRevision: ff1db1214426cf4ceca556a1b9cb265260444adb
  verdict: reproducible
- date: 2020-12-11
  version: "0.18.0"
  apkHash: 9c91d011f3817f35a27276103fea0eae3c6e073e38f148019d53cd7a46371c34
  gitRevision: 15caae034be8a16e851cea5c4bc452ab56341b0f
  verdict: reproducible
- date: 2020-11-27
  version: "0.17.1"
  apkHash: 78e49a893c9e413f56be9717126a53bcb8dc34ccddcdded1a9a0b0f53505c4ec
  gitRevision: 4fb7db792d0f8c31c80b1c3cc358e106c73b7535
  verdict: reproducible
- date: 2020-11-17
  version: "0.17.0"
  apkHash: 80aab3a32f9cdd580331e7414e1229ced5a8ed9eea2f64068a51aa6bf1a70df3
  gitRevision: ed3510cd95d59dc9d71907059dcca63c2539d261
  verdict: reproducible
- date: 2020-10-02
  version: "0.16.2"
  apkHash: 6a4bb767a47990feaa3a844a9b84c0c674b7b575a9d1f9699e0a8bd4152ab4ed
  gitRevision: b70c52974e5c4c1979662517cc2835ff58375316
  verdict: reproducible
- date: 2020-09-24
  version: "0.16.1"
  apkHash: ceb4d72b8ae2358c8779b5e0bba5e08bae08e1524eb9b6c0b2ca86889cc91adc
  gitRevision: f4ef07e88dfb7de43bdd0a7a3eadb6c46c0c3000
  verdict: reproducible
- date: 2020-07-22
  version: "0.16.0"
  apkHash: 6dc469ed865d0fe2cd855c7b227b54692877e4b09f6dc765d56fae80c20a2524
  gitRevision: ad53627c5be906793c9c868da64bf07d83b16de0
  verdict: nonverifiable
- date: 2020-07-15
  version: "0.15.0"
  apkHash: 9fad17afdb38e3ec1e38c4e88faddd479e179d2e7004722e6fb0bd440a6ea851
  gitRevision: cdff061989fc681a1fa928da59fb98a0be7d75b2
  verdict: reproducible
- date: 2020-06-14
  version: "0.14.1"
  apkHash: a45881e3ae8bba31c2fc09ecbb63ce5d200c77512f256971a12e9ab830ae719d
  gitRevision: a28d71826772bcb5d28cad48489b7f91b9c3b882
  verdict: reproducible
- date: 2020-06-07
  version: "0.14.0"
  apkHash: 9abbc4c1b7475c75437c416f5e103d4fd83625f0a7463be6aec545bff86d920d
  gitRevision: b5ebfb9fdedf9b686511645bae3e05fe13aa3d2f
  verdict: nonverifiable
- date: 2020-05-16
  version: "0.13.0"
  apkHash: 20b023949e0572af577beb0df94c6dbaf758be0d7bd323e632392c93c6640f2d
  gitRevision: f2664abad8e41ce1b3e225be0eae63d18a0cc053
  verdict: reproducible
- date: 2020-03-25
  version: "0.12.0"
  apkHash: cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c
  gitRevision: 45359b810b471200750ab0914f6c506054cf1123
  verdict: nonverifiable
- date: 2020-03-21
  version: "0.12.0"
  apkHash: cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c
  gitRevision: 65d19944379884fc3f0b9268e7e83b7dda63b5ba
  verdict: nonverifiable
- date: 2020-01-31
  version: "0.11.0"
  apkHash: f5bd6b218bb5e4fa605ed0c8e3dd9f424baf2656b9008f269d9e34697e0b21c0
  gitRevision: 43d012f4990ca6fed9d4b042224bf8fdd48ff41e
  verdict: reproducible
- date: 2020-01-29
  version: "0.11.0"
  apkHash: f5bd6b218bb5e4fa605ed0c8e3dd9f424baf2656b9008f269d9e34697e0b21c0
  gitRevision: 92e4a67ecc626220965114cd6a4cd67497e3be9f
  verdict: nonverifiable

providerTwitter: unstoppablebyhs
providerLinkedIn: 
providerFacebook: 
providerReddit: UNSTOPPABLEWallet

redirect_from:
  - /io.horizontalsystems.bankwallet/
  - /posts/io.horizontalsystems.bankwallet/
---


Here is the output using our
[test script](https://gitlab.com/walletscrutiny/walletScrutinyCom/blob/master/test.sh)
on the binary from Google Play:

```
Results:
appId:          io.horizontalsystems.bankwallet
signer:         c1899493e440489178b8748851b72cbed50c282aaa8c03ae236a4652f8c4f27b
apkVersionName: 0.18.2
apkVersionCode: 38
apkHash:        5d6cbade37070420b24da421cea93b8591e3709a8d92dc70f2b8b02e1688e7b5

Diff:
Files /tmp/fromPlay_io.horizontalsystems.bankwallet_38/apktool.yml and /tmp/fromBuild_io.horizontalsystems.bankwallet_38/apktool.yml differ
Only in /tmp/fromPlay_io.horizontalsystems.bankwallet_38/original/META-INF: MANIFEST.MF
Only in /tmp/fromPlay_io.horizontalsystems.bankwallet_38/original/META-INF: RELEASEK.RSA
Only in /tmp/fromPlay_io.horizontalsystems.bankwallet_38/original/META-INF: RELEASEK.SF

Revision, tag (and its signature):
```

That's how it should look like to give it the verdict: **reproducible**.

Thank you to Horizontal Systems for a
[donation of $200](https://twitter.com/WalletScrutiny/status/1336651531442155522).
