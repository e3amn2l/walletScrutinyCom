---
title: "Crypto.com l DeFi Wallet"
altTitle: 

users: 100000
appId: com.defi.wallet
launchDate: 2020-05-11
latestUpdate: 2021-01-27
apkVersionName: "1.6.0"
stars: 3.6
ratings: 1096
reviews: 434
size: 23M
website: https://crypto.com/en/defi/
repository: 
issue: 
icon: com.defi.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: company/cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:
  - /com.defi.wallet/
  - /posts/com.defi.wallet/
---


This app's description is promising:

> Decentralized:
> - Gain full control of your crypto and private keys [...]

On their website though we cannot find any links to source code.

Searching their `appId` on GitHub,
[yields nothing](https://github.com/search?q=%22com.defi.wallet%22) neither.

This brings us to the verdict: **not verifiable**.
