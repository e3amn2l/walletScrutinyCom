---
title: "Portals : Crypto Wallet Generator - Bitcoin"
altTitle: 

users: 10000
appId: com.gloath.portalsapp
launchDate: 
latestUpdate: 2020-12-20
apkVersionName: "0.0.4a"
stars: 4.1
ratings: 77
reviews: 39
size: 11M
website: 
repository: 
issue: 
icon: com.gloath.portalsapp.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.gloath.portalsapp/
---


This app is weird and it's not clear why 10k people downloaded it. So the app
does generate wallets but you can't really use those as such ... unless you do
with another software.

So neither from the description nor from trying the app out could we tell what
it actually is supposed to do but there are elements of a vanity-address-generator
which would put it into the wallet category as you would use those private keys
on an actual wallet software and then you would rely on this app generating keys
that it does not share with the provider.

Vanity address generators have been used to scam people before.

As there is some implied claim of not sharing keys with a server but no source
code we consider this to be a closed source wallet and as such it is
**not verifiable**.
