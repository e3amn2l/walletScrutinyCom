---
title: "ILC / BTC Wallet"
altTitle: 

users: 10000
appId: ilcbtc.wallet
launchDate: 2018-02-27
latestUpdate: 2021-01-08
apkVersionName: "4.23"
stars: 4.7
ratings: 253
reviews: 143
size: 13M
website: https://www.ilcoincrypto.com
repository: 
issue: 
icon: ilcbtc.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ILC_B_Project
providerLinkedIn: 
providerFacebook: ILCoinBlockchainProject
providerReddit: ILCoin

redirect_from:
  - /ilcbtc.wallet/
  - /posts/ilcbtc.wallet/
---


The description talking about

> * No Blockchain Download, Install and Run in Seconds

at least suggests that the target audience knows a lot about Bitcoin but there
is no word about being your own bank ...

The linked website is some alt-coin project website with a link back to the
Play Store wallet.

They link to [this GitHub](https://github.com/ILCoinDevTeam) where there is
nothing about an Android wallet.

[GitHub search](https://github.com/search?p=1&q=%22ilcbtc.wallet%22&type=Code)
is neither helpful.

As not even an implicit promise is indicating the contrary we assume the wallet
is custodial and conclude with the verdict: **not verifiable**.
