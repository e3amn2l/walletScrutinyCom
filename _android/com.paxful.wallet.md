---
title: "Paxful Bitcoin Wallet"
altTitle: 

users: 1000000
appId: com.paxful.wallet
launchDate: 2019-04-30
latestUpdate: 2021-01-29
apkVersionName: "1.7.1.534"
stars: 3.6
ratings: 13590
reviews: 8225
size: 28M
website: https://paxful.com/mobile-wallet-app
repository: 
issue: 
icon: com.paxful.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: paxful
providerLinkedIn: 
providerFacebook: paxful
providerReddit: paxful

redirect_from:
  - /paxful/
  - /com.paxful.wallet/
  - /posts/2019/11/paxful/
  - /posts/com.paxful.wallet/
---


According to their Playstore description:

> The bitcoin wallet app is also the ultimate companion tool to Paxful, one of
the world’s biggest peer-to-peer bitcoin marketplaces.

> Track your open trades on Paxful so you know the current status of your most
recent transactions as you buy and sell bitcoin

which sounds like a tool to manage coins on [paxful](https://paxful.com/).

Nowhere on the Playstore or on their website did we find a link to source code.

[Nowhere on GitHub](https://github.com/search?p=3&q=%22com.paxful.wallet%22) did
we find their applicationId `com.paxful.wallet` as an actual applicationId in
an Android project.

Our verdict thus is: **not verifiable** and probably custodial.
