---
title: "Hebe Wallet"
altTitle: 

users: 1000
appId: com.hebeblock.hebewallet
launchDate: 
latestUpdate: 2021-01-30
apkVersionName: "1.2.51"
stars: 3.3
ratings: 7
reviews: 4
size: 37M
website: https://hebe.cc/
repository: 
issue: 
icon: com.hebeblock.hebewallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BlockHebe
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /posts/com.hebeblock.hebewallet/
---


In the description the provider claims:

> Hebe Wallet is a decentralized wallet that supports local transaction
  signatures, so your mnemonics will never be sent over the internet.

so it's not custodial but we can't find any source code. This app is
**not verifiable**.
