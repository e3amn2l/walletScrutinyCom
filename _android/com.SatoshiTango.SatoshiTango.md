---
title: "SatoshiTango"
altTitle: 

users: 50000
appId: com.SatoshiTango.SatoshiTango
launchDate: 
latestUpdate: 2021-01-08
apkVersionName: "3.4.15"
stars: 4.0
ratings: 3456
reviews: 1912
size: 70M
website: http://www.satoshitango.com
repository: 
issue: 
icon: com.SatoshiTango.SatoshiTango.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: satoshitango
providerLinkedIn: 
providerFacebook: satoshitangoargentina
providerReddit: 

redirect_from:
  - /com.SatoshiTango.SatoshiTango/
---


> Buy and sell BTC, ETH, LTC, XRP and BCH and monitor your balance and transactions.
> 
> Pay in local currency and hold a balance in fiat currency.
> 
> Store your cryptos and much more!

sounds like a wallet that also supports Bitcoin but there is not much
information on who gets to control the keys. They explain what a private keys is
in the FAQ though :)

We have to assume this is a custodial offering and thus **not verifiable**.
