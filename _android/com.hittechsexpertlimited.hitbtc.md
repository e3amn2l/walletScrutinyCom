---
title: "HitBTC – Cryptocurrency Exchange & Trading BTC App"
altTitle: 

users: 100000
appId: com.hittechsexpertlimited.hitbtc
launchDate: 
latestUpdate: 2021-01-22
apkVersionName: "3.0.6"
stars: 3.9
ratings: 869
reviews: 420
size: 11M
website: https://hitbtc.com
repository: 
issue: 
icon: com.hittechsexpertlimited.hitbtc.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: hitbtc
providerLinkedIn: 
providerFacebook: hitbtc
providerReddit: hitbtc

redirect_from:
  - /com.hittechsexpertlimited.hitbtc/
  - /posts/com.hittechsexpertlimited.hitbtc/
---


On Google Play this app claims

> **High-Level Security**
  Don’t let anybody sneak into your trade: account access is strictly via API
  keys and PIN-code. Plus, advanced encryption technology and highly protected
  cold storage.

Which means it is a custodial service and thus **not verifiable**.