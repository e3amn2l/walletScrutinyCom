---
title: "BuyCoins - Buy & Sell Bitcoin, Ethereum, Litecoin"
altTitle: 

users: 50000
appId: africa.bitkoin.buycoins
launchDate: 
latestUpdate: 2020-12-16
apkVersionName: "5.3.0"
stars: 3.0
ratings: 545
reviews: 381
size: 11M
website: https://buycoins.africa
repository: 
issue: 
icon: africa.bitkoin.buycoins.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: buycoins_africa
providerLinkedIn: 
providerFacebook: buycoinsafrica
providerReddit: 

redirect_from:
  - /africa.bitkoin.buycoins/
  - /posts/africa.bitkoin.buycoins/
---


This app claims to have a secure wallet:

> * STORE YOUR CRYPTOCURRENCIES IN OUR SECURE WALLET - Store your
  cryptocurrencies in our secure wallet free of charge

This can be interpreted in different ways. Further down again:

> Why use Buycoins?
> 
> * SECURE WALLET

Although the [FAQ](https://help.buycoins.africa/article/ujx40uowhw-wallet-address-best-practices)
doesn't make clear claims neither, this:

> 4. Check the transaction on a blockchain explorer
> 
> After you are sure the cryptocurrency has been sent to your wallet, check the
  progress of the transaction on a blockchain explorer. You can do this using
  either your wallet address or the transaction hash. There, you can see how
  many confirmations the transaction has. Ideally, your coins should reflect in
  your BuyCoins wallet after 3 confirmations.

sounds like your coins get credited once the exchange is sure you have them
which means they are custodial which means the wallet is **not verifiable**.
