---
title: "BitKeep Wallet Pro"
altTitle: 

users: 10000
appId: com.bitkeep.wallet
launchDate: 
latestUpdate: 2021-01-26
apkVersionName: "5.0.6"
stars: 3.9
ratings: 196
reviews: 85
size: 27M
website: https://bitkeep.com
repository: 
issue: 
icon: com.bitkeep.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BitKeepOS
providerLinkedIn: 
providerFacebook: bitkeep
providerReddit: 

redirect_from:
  - /com.bitkeep.wallet/
---


The description

> BitKeep is the largest multi-chain wallet, which can manage thousands of digital currencies, including Bitcoin, Ethereum, EOS, etc., is completely decentralized, no registration is required, one-click use, a set of mnemonic manages all assets, and the data is completely own management.
> 
> 1. Super Decentralized<br>
  No registration, one-click use. BitKeep as a window for you to know the blockchain. A set of mnemonics manages all digital currencies. The data comes from the blockchain. Just backup the mnemonics and your assets will never be lost.

could be read as "this wallet is non-custodial" but it's not explicit. Maybe the
website has more to say on that topic ...

We are not sure what this claim actually does mean:

> **Security Guarantee**<br>
  Investing tens of millions of dollars, original DESM encryption algorithm, industrial-grade encryption security, can double-check to ensure the security of your digital assets, even if a hacker invades your mobile phone, you cannot steal your assets.

As there is no source code to be found anywhere, this app is at best a
non-custodial closed source wallet and as such **not verifiable**.