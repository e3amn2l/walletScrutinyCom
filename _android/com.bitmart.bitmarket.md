---
title: "BitMart - Cryptocurrency Exchange"
altTitle: 

users: 50000
appId: com.bitmart.bitmarket
launchDate: 
latestUpdate: 2021-02-05
apkVersionName: "2.4.4"
stars: 3.4
ratings: 696
reviews: 405
size: 31M
website: https://www.bitmart.com
repository: 
issue: 
icon: com.bitmart.bitmarket.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BitMartExchange
providerLinkedIn: company/bitmart
providerFacebook: bitmartexchange
providerReddit: BitMartExchange

redirect_from:
  - /com.bitmart.bitmarket/
  - /posts/com.bitmart.bitmarket/
---


On Google Play we read (emphasize ours):

> Security and Stability: BitMart adopts an advanced multi-layer and
  multi-cluster system architecture to ensure the security, stability, and
  scalability of the system. Its blockchain nodes apply **hot/cold wallet**
  technology while the operating modes of secret keys and addresses are
  optimized according to the features of blockchain to ensure the security of
  users' assets.

A "hot" wallet is online, a "cold" wallet is offline. Your phone is certainly
not "cold", so it's them who hold the keys. As a custodial service the app is
**not verifiable**.
