---
title: "FreeWallet"
altTitle: 

users: 5000
appId: io.freewallet.mobile
launchDate: 2016-09-01
latestUpdate: 2019-03-17
apkVersionName: "0.1.9"
stars: 4.1
ratings: 56
reviews: 29
size: 6.7M
website: https://freewallet.io
repository: https://github.com/jdogresorg/freewallet-mobile
issue: https://github.com/jdogresorg/freewallet-mobile/issues/34
icon: io.freewallet.mobile.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-05
reviewStale: false
signer: 
reviewArchive:


providerTwitter: freewallet
providerLinkedIn: 
providerFacebook: freewallet.io
providerReddit: 

redirect_from:
  - /io.freewallet.mobile/
---


The provider of this Freewallet reached out to us to stress that freewallet.io
was not the same as freewallet.org which is spamming Google Play with many
wallets and we have reviewed three of those here, too:

* [Bitcoin Wallet. Buy & Exchange BTC coin－Freewallet](/btc.org.freewallet.app/)
  is custodial with 500k downloads.
* [Bitcoin & Crypto Blockchain Wallet: Freewallet](/mw.org.freewallet.app/) is
  custodial, with 100k downloads.
* [Lite HD Wallet – Your Coin Base](/org.freewallet.lite.android/) is probably
  also custodial, with only 500 downloads so far.

He sais that his wallet is non-custodial and open source and indeed we see those
same claims on Google Play.

On the website we read:

> **Open Source**<br>
  FreeWallet Mobile is open-source, and available for anyone to fork or review, so you know that it works exactly the way that it is supposed to.

and indeed we find [a repository](https://github.com/jdogresorg/freewallet-mobile).
It had no changes for two years and also Google Play does not report any changes
since March 2019 but this looks like open source.

Unfortunately there is no build instructions so we cannot reproduce the app and
it remains **not verifiable**.
