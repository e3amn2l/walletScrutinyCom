---
title: "Bithumb Singapore - Global Cryptocurrency Exchange"
altTitle: 

users: 10000
appId: com.bitholic.rdmchain.bitholic
launchDate: 
latestUpdate: 2020-08-05
apkVersionName: "1.0.9"
stars: 4.3
ratings: 153
reviews: 100
size: 2.0M
website: https://www.bithumbsg.com
repository: 
issue: 
icon: com.bitholic.rdmchain.bitholic.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.bitholic.rdmchain.bitholic/
  - /posts/com.bitholic.rdmchain.bitholic/
---


This app is an interface for an exchange. Your coins are stored on their
infrastructure. As a custodial service it is **not verifiable**.