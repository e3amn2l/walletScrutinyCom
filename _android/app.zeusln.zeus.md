---
title: "Zeus: Bitcoin/Lightning Wallet"
altTitle: 

users: 500
appId: app.zeusln.zeus
launchDate: 
latestUpdate: 2020-10-31
apkVersionName: "0.4.0"
stars: 4.2
ratings: 14
reviews: 8
size: 26M
website: 
repository: 
issue: 
icon: app.zeusln.zeus.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-07-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /app.zeusln.zeus/
  - /posts/app.zeusln.zeus/
---


