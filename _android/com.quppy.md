---
title: "Quppy Wallet - bitcoin, crypto and euro payments"
altTitle: 

users: 100000
appId: com.quppy
launchDate: 
latestUpdate: 2021-01-22
apkVersionName: "1.0.44"
stars: 4.2
ratings: 1978
reviews: 954
size: 15M
website: https://quppy.com
repository: 
issue: 
icon: com.quppy.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: QuppyPay
providerLinkedIn: company/quppy
providerFacebook: quppyPay
providerReddit: 

redirect_from:
  - /com.quppy/
---


This provider loses no word on security or where the keys are stored. We assume
it is a custodial offering and therefore **not verifiable**.