---
title: "Bcoiner - Free Bitcoin Wallet"
altTitle: 

users: 100000
appId: com.bcoiner.webviewapp
launchDate: 2014-12-01
latestUpdate: 2018-01-05
apkVersionName: "1.3.2"
stars: 4.2
ratings: 2372
reviews: 1190
size: 2.2M
website: https://bcoiner.com
repository: 
issue: 
icon: com.bcoiner.webviewapp.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-03-28
reviewStale: false
signer: 
reviewArchive:
- date: 2019-11-12
  version: "1.3.2"
  apkHash: 
  gitRevision: 372c9c03c6422faed457f1a9975d7cab8f13d01f
  verdict: nosource

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /bcoiner/
  - /com.bcoiner.webviewapp/
  - /posts/2019/11/bcoiner/
  - /posts/com.bcoiner.webviewapp/
---


Bcoiner - Free Bitcoin Wallet
does not share clear information it looks custodial and absent source code it is
definitely **not verifiable**.

Other observations
------------------

* The ratings are ... suspicious. Blocks of 5 star and 1 star claiming 5 star
  are fake and scam accusations.
