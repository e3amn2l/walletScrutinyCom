---
title: "ProBit Global: Buy & Sell Bitcoin. Crypto Exchange"
altTitle: 

users: 100000
appId: com.probit.app.android2.release.global
launchDate: 
latestUpdate: 2021-01-27
apkVersionName: "1.30.1"
stars: 3.5
ratings: 2819
reviews: 1871
size: 17M
website: https://www.probit.com
repository: 
issue: 
icon: com.probit.app.android2.release.global.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ProBit_Exchange
providerLinkedIn: company/probit-exchange
providerFacebook: probitexchange
providerReddit: 

redirect_from:
  - /com.probit.app.android2.release.global/
---


Probit appears to also and mainly be an exchange and as we can't find claims to
the contrary, we assume this app is a custodial offering and thus **not verifiable**.
