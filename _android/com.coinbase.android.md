---
title: "Coinbase – Buy & Sell Bitcoin. Crypto Wallet"
altTitle: 

users: 10000000
appId: com.coinbase.android
launchDate: 2013-03-01
latestUpdate: 2021-02-05
apkVersionName: "9.9.1"
stars: 4.2
ratings: 290522
reviews: 109557
size: Varies with device
website: https://coinbase.com
repository: 
issue: 
icon: com.coinbase.android.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-10-30
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coinbase
providerLinkedIn: company/coinbase
providerFacebook: Coinbase
providerReddit: CoinBase

redirect_from:
  - /coinbase/
  - /com.coinbase.android/
  - /posts/2019/10/coinbase/
  - /posts/com.coinbase.android/
---


The Coinbase app, not to be confused with [Coinbas Wallet](/coinbasewallet)
is one of the top two Bitcoin "wallets" on Google Play but beyond the
name, nothing indicates this app to be an actual wallet.

Historically Coinbase was an exchange and like almost all exchanges, they
allowed to hold Bitcoins in trading accounts. Later the Android app was released
and called a "wallet".

(On another historical note, Brian Armstrong, a co-founder of Coinbase did release
an [actual Bitcoin Wallet](https://github.com/barmstrong/bitcoin-android) back
[in June 2011](https://thenextweb.com/mobile/2011/07/06/bitcoin-payments-go-mobile-with-bitcoin-for-android/).
It was open source and downloaded the full blockchain to your phone.)

As the wallet setup does not involve a way to backup private keys, we assume those
private keys are under the sole control of Coinbase, making it
a custodial wallet or non-wallet.

Verdict: This app is **not verifiable**.