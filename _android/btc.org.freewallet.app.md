---
title: "Bitcoin Wallet. Buy & Exchange BTC coin－Freewallet"
altTitle: 

users: 500000
appId: btc.org.freewallet.app
launchDate: 2016-06-13
latestUpdate: 2020-04-21
apkVersionName: "2.5.2"
stars: 4.1
ratings: 4165
reviews: 1989
size: 7.2M
website: https://freewallet.org
repository: 
issue: 
icon: btc.org.freewallet.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-12
reviewStale: false
signer: 
reviewArchive:


providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: Freewallet_org

redirect_from:
  - /btc.org.freewallet.app/
  - /posts/btc.org.freewallet.app/
---


According to their description on Google Play, this is a custodial app:

> The Freewallet team keeps most of our customers’ coins in offline cold storage
to ensure the safety of your funds.

Our verdict: This app is **not verifiable**.
