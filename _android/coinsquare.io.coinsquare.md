---
title: "Coinsquare"
altTitle: 

users: 50000
appId: coinsquare.io.coinsquare
launchDate: 
latestUpdate: 2020-07-29
apkVersionName: "2.13.8"
stars: 2.5
ratings: 396
reviews: 300
size: 8.2M
website: https://coinsquare.com
repository: 
issue: 
icon: coinsquare.io.coinsquare.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coinsquare
providerLinkedIn: 
providerFacebook: coinsquare.io
providerReddit: 

redirect_from:
  - /coinsquare.io.coinsquare/
  - /posts/coinsquare.io.coinsquare/
---


This is the interface for an exchange. In the description we read:

> We are SSL and 2FA enabled, with a 95% cold storage policy on all digital
  currency, and run multiple encrypted and distributed backups every day.

which means this is a custodial service and thus **not verifiable**.