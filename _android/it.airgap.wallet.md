---
title: "AirGap Wallet - Tezos, Cosmos, Ethereum, Bitcoin"
altTitle: 

users: 1000
appId: it.airgap.wallet
launchDate: 2018-08-06
latestUpdate: 2020-06-05
apkVersionName: "3.2.3"
stars: 4.3
ratings: 40
reviews: 20
size: 12M
website: https://www.airgap.it
repository: 
issue: 
icon: it.airgap.wallet.png
bugbounty: 
verdict: nowallet # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

redirect_from:
  - /it.airgap.wallet/
  - /posts/it.airgap.wallet/
---


This appears to not be a wallet as to our understanding, it does not hold any
private keys but delegates that part to [AirGap Vault](/it.airgap.vault/)
without which it does not work.

> **AirGap Vault**, the private key is generated and securely stored in the
  AirGap Vault app. **You have to install AirGap Vault to use AirGap Wallet**
  https://play.google.com/store/apps/details?id=it.airgap.vault
  