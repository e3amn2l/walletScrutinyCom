---
title: "Azara Crypto & Bitcoin Wallet: Buy, Sell, Exchange"
altTitle: 

users: 100
appId: ai.azara.wallet
launchDate: 
latestUpdate: 2020-11-11
apkVersionName: "0.01.05"
stars: 4.0
ratings: 5
reviews: 3
size: 45M
website: 
repository: 
issue: 
icon: ai.azara.wallet.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /ai.azara.wallet/
---


