---
title: "Fact Wallet - Bitcoin and cryptocurrency Wallet"
altTitle: 

users: 100
appId: com.factwallet.crypto.factwallet
launchDate: 
latestUpdate: 2020-11-13
apkVersionName: "1.4"
stars: 0.0
ratings: 
reviews: 
size: 3.8M
website: 
repository: 
issue: 
icon: com.factwallet.crypto.factwallet.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.factwallet.crypto.factwallet/
---


