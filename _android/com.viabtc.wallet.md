---
title: "ViaWallet - Multi-chain Wallet"
altTitle: 

users: 10000
appId: com.viabtc.wallet
launchDate: 2019-05-15
latestUpdate: 2021-01-21
apkVersionName: "2.2.4"
stars: 4.5
ratings: 182
reviews: 77
size: 50M
website: https://viawallet.com
repository: 
issue: 
icon: com.viabtc.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-27
reviewStale: true
signer: 
reviewArchive:


providerTwitter: viawallet
providerLinkedIn: 
providerFacebook: ViaWallet
providerReddit: 

redirect_from:
  - /com.viabtc.wallet/
  - /posts/com.viabtc.wallet/
---


This app's description contains

> Users' self-control for private key to manage assets

which sounds like a claim to being non-custodial.

Also on their [FAQ](https://support.viawallet.com/hc/en-us/articles/900000212786-Is-my-digital-assets-safely-stored-in-ViaWallet-)
we read:

> **Is my digital assets safely stored in ViaWallet?**
> ViaWallet is a subsidiary brand of ViaBTC, which was founded in May 2016 as an
  innovation-intensive startup dedicated to cryptocurrency with rich
  technological prowess and experience in global operation of blockchain
  industry. With the most state-of-the-art Fintech, we aims to provide a
  self-controlled and easy-accessible multi-cryptocurrency wallet across
  devices, safeguarded with industry-leading security for your digital assets.

which says "self-controlled" but nowhere do we find a hint at this wallet's
source code. We assume it's claiming to be non-custodial but remain with the
verdict: **not verifiable**.