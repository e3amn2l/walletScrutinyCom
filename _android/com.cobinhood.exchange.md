---
title: "COBINHOOD - Zero Fees Bitcoin Exchange & Wallet"
altTitle: 

users: 50000
appId: com.cobinhood.exchange
launchDate: 
latestUpdate: 2019-04-29
apkVersionName: "3.50.1"
stars: 2.0
ratings: 639
reviews: 376
size: 7.7M
website: https://cobinhood.com
repository: 
issue: 
icon: com.cobinhood.exchange.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.cobinhood.exchange/
  - /posts/com.cobinhood.exchange/
---


This exchange and wallet claims:

> - High-security crypto-asset storage: Your crypto-asset deposits will be
  backed by insurance and partially stored in an offline multisig wallet.

Which means it is custodial and thus **not verifiable**.