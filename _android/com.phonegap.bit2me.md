---
title: "Bit2Me - Buy and Sell Cryptocurrencies"
altTitle: 

users: 10000
appId: com.phonegap.bit2me
launchDate: 
latestUpdate: 2021-02-01
apkVersionName: "2.0.48"
stars: 4.5
ratings: 798
reviews: 525
size: 16M
website: https://bit2me.com
repository: 
issue: 
icon: com.phonegap.bit2me.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bit2me
providerLinkedIn: company/bit2me
providerFacebook: bit2me
providerReddit: 

redirect_from:
  - /com.phonegap.bit2me/
  - /posts/com.phonegap.bit2me/
---


This appears to be the interface for an exchange. We could not find any claims
about you owning your keys. As a custodial service it is **not verifiable**.
