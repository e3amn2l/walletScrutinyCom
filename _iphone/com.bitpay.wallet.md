---
title: "BitPay – Buy Crypto"
altTitle: 

appId: com.bitpay.wallet
idd: 1149581638
released: 2016-10-24
updated: 2021-02-05
version: "12.0.11"
score: 4.05905
reviews: 999
size: 87375872
developerWebsite: https://bitpay.com
repository: 
issue: 
icon: com.bitpay.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

