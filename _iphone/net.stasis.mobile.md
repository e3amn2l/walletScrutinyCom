---
title: "STASIS Stablecoin Wallet"
altTitle: 

appId: net.stasis.mobile
idd: 1371949230
released: 2018-07-06
updated: 2020-09-01
version: "7.8"
score: 3.66667
reviews: 3
size: 22332416
developerWebsite: https://stasis.net
repository: 
issue: 
icon: net.stasis.mobile.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

