---
title: "Trust: Crypto & Bitcoin Wallet"
altTitle: 

appId: com.sixdays.trust
idd: 1288339409
released: 2017-09-27
updated: 2021-01-18
version: "5.11"
score: 4.79398
reviews: 18542
size: 40117248
developerWebsite: https://trustwallet.com
repository: 
issue: 
icon: com.sixdays.trust.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: trustwalletapp
providerLinkedIn: 
providerFacebook: trustwalletapp
providerReddit: trustapp

redirect_from:

---

On the App Store they claim:

> Best digital wallet to securely store private keys on your device

While this is not an explicit claim of them not having a copy of those keys, it
is at least strongly implied.

On their website they are more specific:

> **Private & Secure**<br>
  Only you can access your wallet. We don’t collect any personal data.

but as with [their Android app](/android/com.wallet.crypto.trustapp/) there is
no public source of the iPhone app neither.

Without public code the verdict is: **not verifiable**.
