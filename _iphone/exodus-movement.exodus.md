---
title: "Exodus: Crypto Bitcoin Wallet"
altTitle: 

appId: exodus-movement.exodus
idd: 1414384820
released: 2019-03-23
updated: 2021-02-07
version: "21.2.5"
score: 4.68132
reviews: 6301
size: 28465152
developerWebsite: https://www.exodus.io/mobile
repository: 
issue: 
icon: exodus-movement.exodus.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: exodus_io
providerLinkedIn: 
providerFacebook: exodus.io
providerReddit: 

redirect_from:

---

Just like [their Android wallet](/android/exodusmovement.exodus/), this app is
closed source and thus **not verifiable**.