---
title: "Unstoppable Wallet"
altTitle: 

appId: io.horizontalsystems.bank-wallet
idd: 1447619907
released: 2019-01-10
updated: 2020-12-11
version: "0.18"
score: 4.72864
reviews: 199
size: 45149184
developerWebsite: https://horizontalsystems.io/
repository: https://github.com/horizontalsystems/unstoppable-wallet-ios
issue: 
icon: io.horizontalsystems.bank-wallet.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: false
signer: 
reviewArchive:


providerTwitter: unstoppablebyhs
providerLinkedIn: 
providerFacebook: 
providerReddit: UNSTOPPABLEWallet

redirect_from:

---

The provider claims:

> A non-custodial wallet without third party risk.

and we found the source code
[here](https://github.com/horizontalsystems/unstoppable-wallet-ios)
but so far nobody reproduced the build, so the claim is **not verifiable**.
