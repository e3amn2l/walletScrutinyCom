---
title: "Coinbase Wallet"
altTitle: 

appId: org.toshi.distribution
idd: 1278383455
released: 2017-09-27
updated: 2021-02-01
version: "23.4"
score: 4.69146
reviews: 17278
size: 136258560
developerWebsite: https://wallet.coinbase.com
repository: 
issue: 
icon: org.toshi.distribution.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinbaseWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This is the iPhone version of the
[Android Coinbase Wallet — Crypto Wallet & DApp Browser](/android/org.toshi).

Just like the Android version, this wallet is **not verifiable**.