---
title: "Cryptonator"
altTitle: 

appId: com.cryptonator.ios
idd: 1463793201
released: 2019-06-11
updated: 2021-01-27
version: "4.1.4"
score: 3.375
reviews: 8
size: 79916032
developerWebsite: https://www.cryptonator.com
repository: 
issue: 
icon: com.cryptonator.ios.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

