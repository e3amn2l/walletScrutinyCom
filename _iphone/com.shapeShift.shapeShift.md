---
title: "ShapeShift: Buy & Trade Crypto"
altTitle: 

appId: com.shapeShift.shapeShift
idd: 996569075
released: 2015-06-09
updated: 2021-02-03
version: "2.10.0"
score: 3.11905
reviews: 336
size: 76918784
developerWebsite: https://shapeshift.com
repository: 
issue: 
icon: com.shapeShift.shapeShift.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

