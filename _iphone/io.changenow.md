---
title: "ChangeNOW Crypto Exchange"
altTitle: 

appId: io.changenow
idd: 1518003605
released: 2020-06-29
updated: 2020-11-22
version: "1.3.0"
score: 4.62791
reviews: 86
size: 20734976
developerWebsite: 
repository: 
issue: 
icon: io.changenow.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

