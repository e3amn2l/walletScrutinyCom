---
title: "Edge - Crypto & Bitcoin Wallet"
altTitle: 

appId: co.edgesecure.app
idd: 1344400091
released: 2018-02-09
updated: 2021-02-06
version: "2.0.1"
score: 4.375
reviews: 520
size: 60596224
developerWebsite: https://edge.app
repository: https://github.com/EdgeApp/edge-react-gui
issue: 
icon: co.edgesecure.app.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: edgewallet
providerLinkedIn: company/3609678
providerFacebook: 
providerReddit: 

redirect_from:

---

On the App Store the provider claims:

> Edge is a powerful and easy to use cryptocurrency wallet that allows users to
  easily control their own private keys with the familiarity and ease of mobile banking. 

and

> The Edge app has open-source code to ensure the highest level of security and
  privacy.

and indeed on their website we find a link to GitHub and assume
[this](https://github.com/EdgeApp/edge-react-gui) is the app's repository.

Unfortunately apps on iPhone can't be reproduced so far, so the app is
**not verifiable**.
