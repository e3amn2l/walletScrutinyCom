---
title: "Remitano"
altTitle: 

appId: com.remitano.remitano
idd: 1116327021
released: 2016-05-28
updated: 2021-02-05
version: "5.6.1"
score: 4.76331
reviews: 5822
size: 57526272
developerWebsite: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: remitano
providerLinkedIn: company/Remitano
providerFacebook: remitano
providerReddit: 

redirect_from:

---

This app is an interface to an exchange which holds your coins. On the App Store
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.