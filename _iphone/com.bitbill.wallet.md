---
title: "Ownbit - Blockchain Wallet"
altTitle: 

appId: com.bitbill.wallet
idd: 1321798216
released: 2018-02-07
updated: 2021-01-24
version: "4.26.0"
score: 4.54167
reviews: 48
size: 108550144
developerWebsite: 
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

