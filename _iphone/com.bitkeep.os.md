---
title: "BitKeep"
altTitle: 

appId: com.bitkeep.os
idd: 1395301115
released: 2018-09-26
updated: 2021-01-27
version: "5.0.6"
score: 3.66667
reviews: 6
size: 59446272
developerWebsite: https://bitkeep.com
repository: 
issue: 
icon: com.bitkeep.os.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

