---
title: "Crypto.com l DeFi Wallet"
altTitle: 

appId: com.defi.wallet
idd: 1512048310
released: 2020-05-20
updated: 2021-01-28
version: "1.6.0"
score: 3.15625
reviews: 128
size: 63419392
developerWebsite: https://crypto.com/en/defi/wallet/
repository: 
issue: 
icon: com.defi.wallet.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: company/cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

This app's description is promising:

> Decentralized:
> - Gain full control of your crypto and private keys [...]

On their website though we cannot find any links to source code.

Searching their `appId` on GitHub,
[yields nothing](https://github.com/search?q=%22com.defi.wallet%22) neither.

This brings us to the verdict: **not verifiable**.
