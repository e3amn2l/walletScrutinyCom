---
title: "Melis Wallet"
altTitle: 

appId: io.melis.clientwallet
idd: 1176840794
released: 2017-06-07
updated: 2019-06-06
version: "1.5.40"
score: 
reviews: 
size: 14932992
developerWebsite: http://melis.io
repository: 
issue: 
icon: io.melis.clientwallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-21
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

On the App Store this provider makes no claims about the app being non-custodial
or source code being public but on their website we find:

> **The safest wallet**<br>
  With Melis you have the complete control of your bitcoins and private keys,
  you can define spending limits policies and make use of two or more factors
  authentication.

which is a clear claim but the multi factor authentication is a bit worrying as
that might rely on giving their server power to inhibit your transactions.

We also find:

> Melis is open source, published on [GitHub](https://github.com/melis-wallet).

but none of the repositories there has an obvious mobile wallet name. If the
source code is somewhere then this is probably an unfortunate way of getting the
verdict **not verifiable** due to lack of source.
