---
title: "LiteBit - Buy & sell Bitcoin"
altTitle: 

appId: com.litebit.app
idd: 1448841440
released: 2019-08-20
updated: 2021-02-03
version: "2.15.2"
score: 4.2
reviews: 5
size: 126650368
developerWebsite: https://www.litebit.eu/en/
repository: 
issue: 
icon: com.litebit.app.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

