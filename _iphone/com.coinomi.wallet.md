---
title: "Coinomi Wallet"
altTitle: 

appId: com.coinomi.wallet
idd: 1333588809
released: 2018-03-22
updated: 2020-12-22
version: "1.9.1"
score: 4.34798
reviews: 569
size: 104544256
developerWebsite: https://www.coinomi.com
repository: 
issue: 
icon: com.coinomi.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

