---
title: "Flare Wallet"
altTitle: 

appId: org.flarewallet.flare
idd: 1496651406
released: 2020-02-11
updated: 2021-01-02
version: "1.3.9"
score: 4.16129
reviews: 31
size: 24001536
developerWebsite: https://flarewallet.io
repository: 
issue: 
icon: org.flarewallet.flare.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

