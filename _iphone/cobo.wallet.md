---
title: "Cobo Crypto Wallet: BTC & DASH"
altTitle: 

appId: cobo.wallet
idd: 1406282615
released: 2018-08-05
updated: 2020-12-15
version: "4.26"
score: 3.7381
reviews: 42
size: 43097088
developerWebsite: https://cobo.com
repository: 
issue: 
icon: cobo.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

