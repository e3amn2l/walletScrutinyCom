---
title: "Buy Bitcoin - Spot Wallet app"
altTitle: 

appId: tech.spotapp.spot
idd: 1390560448
released: 2018-08-07
updated: 2021-02-05
version: "3.0.5"
score: 4.61935
reviews: 2346
size: 83529728
developerWebsite: https://spot-bitcoin.com
repository: 
issue: 
icon: tech.spotapp.spot.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

