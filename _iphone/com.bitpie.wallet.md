---
title: "Bitpie-Universal Crypto Wallet"
altTitle: 

appId: com.bitpie.wallet
idd: 1481314229
released: 2019-10-01
updated: 2021-02-03
version: "5.0.013"
score: 3.47059
reviews: 17
size: 275618816
developerWebsite: 
repository: 
issue: 
icon: com.bitpie.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

