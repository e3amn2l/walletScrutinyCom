---
title: "Coin Bitcoin Wallet"
altTitle: 

appId: com.coinspace.wallet
idd: 980719434
released: 2015-12-14
updated: 2021-02-04
version: "3.0.7"
score: 4.48352
reviews: 91
size: 29129728
developerWebsite: https://coin.space/
repository: https://github.com/CoinSpace/CoinSpace
issue: 
icon: com.coinspace.wallet.jpg
bugbounty: https://www.openbugbounty.org//bugbounty/CoinAppWallet/
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coinappwallet
providerLinkedIn: company/coin-space
providerFacebook: coinappwallet
providerReddit: 

redirect_from:

---

On the website the provider claims:

> keys are stored locally, on your device

and there is a public source repository on GitHub but as iPhone apps are
all currently not reproducible, the app remains **not verifiable**.