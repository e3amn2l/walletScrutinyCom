---
title: "Infinito Wallet - Crypto Safe"
altTitle: 

appId: io.infinito.wallet
idd: 1315572736
released: 2018-01-17
updated: 2020-12-16
version: "2.35.0"
score: 4.34524
reviews: 168
size: 105237504
developerWebsite: 
repository: 
issue: 
icon: io.infinito.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

