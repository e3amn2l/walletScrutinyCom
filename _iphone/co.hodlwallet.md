---
title: "HODL Wallet : Bitcoin Wallet"
altTitle: 

appId: co.hodlwallet
idd: 1382342568
released: 2018-08-01
updated: 2020-05-19
version: "1.12"
score: 4.44186
reviews: 86
size: 42128384
developerWebsite: https://hodlwallet.com
repository: https://github.com/hodlwallet/hodl-wallet-ios
issue: 
icon: co.hodlwallet.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-20
reviewStale: false
signer: 
reviewArchive:


providerTwitter: hodlwallet
providerLinkedIn: 
providerFacebook: hodlwallet
providerReddit: 

redirect_from:

---

On the App Store the provider claims:

> Your Bitcoin are stored on your device and backed up to a Backup Recovery Key
  when you create a wallet. This means HODL Wallet can never stop you from
  accessing or sending your funds.

which is not very clear. The Backup part is a bit concerning but if they had
access to that, the second sentence would be wrong. This should thus be a claim
of being non-custodial.

> HODL Wallet is free, open source

so in their source repository one could check the claim but as iPhone apps are
all currently not reproducible, the app remains **not verifiable**.