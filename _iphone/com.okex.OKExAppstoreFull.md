---
title: "OKEx - Bitcoin,Cryptocurrency"
altTitle: 

appId: com.okex.OKExAppstoreFull
idd: 1327268470
released: 2018-01-04
updated: 2021-02-07
version: "4.6.6"
score: 4.9865
reviews: 16592
size: 272896000
developerWebsite: https://www.okex.com
repository: 
issue: 
icon: com.okex.OKExAppstoreFull.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: OKEx
providerLinkedIn: 
providerFacebook: okexofficial
providerReddit: OKEx

redirect_from:

---

On their website we find:

> **Institutional-grade Security**<br>
  Cold wallet technology developed by the world's top security team adopts a
  multi-security-layer mechanism to safeguard your assets

"Cold wallet technology" means this is a custodial offering and therefore
**not verifiable**.
