---
title: "Mycelium Bitcoin Wallet"
altTitle: 

appId: com.mycelium.wallet-ios
idd: 943912290
released: 2014-12-17
updated: 2021-01-09
version: "1.16"
score: 2.21695
reviews: 295
size: 7785472
developerWebsite: https://wallet.mycelium.com
repository: https://github.com/mycelium-com/wallet-ios
issue: 
icon: com.mycelium.wallet-ios.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: MyceliumCom
providerLinkedIn: company/mycelium
providerFacebook: myceliumcom
providerReddit: mycelium

redirect_from:

---

**Disclaimer**: The authors of this project have contributed to Mycelium Android.

This app is the iPhone version of [Mycelium Android](/mycelium/).
It has also [public source code](https://github.com/mycelium-com/wallet-ios)
which is independent of the Android version.

The provider claims:

> 100% control over your private keys, they never leave your device unless you
  export them.

but so far nobody reproduced the build, so the claim is **not verifiable**.
