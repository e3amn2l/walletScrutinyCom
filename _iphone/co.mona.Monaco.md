---
title: "Crypto.com - Buy Bitcoin Now"
altTitle: 

appId: co.mona.Monaco
idd: 1262148500
released: 2017-08-31
updated: 2021-02-03
version: "3.83"
score: 4.43952
reviews: 8598
size: 278053888
developerWebsite: https://crypto.com/
repository: 
issue: 
icon: co.mona.Monaco.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-11
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: company/cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

As their [version for Android](/android/co.mona.android) this app is custodial
and thus **not verifiable**.
