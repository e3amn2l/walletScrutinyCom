---
title: "Cash App"
altTitle: 

appId: com.squareup.cash
idd: 711923939
released: 2013-10-16
updated: 2021-02-03
version: "3.32.2"
score: 4.7199
reviews: 1234762
size: 207197184
developerWebsite: https://cash.app
repository: 
issue: 
icon: com.squareup.cash.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

On their website the provider claims:

> **Coin Storage**<br>
  Your Bitcoin balance is securely stored in our offline system

which means it is custodial.
