---
title: "Abra: Bitcoin & Crypto Wallet"
altTitle: 

appId: com.goabra.abra
idd: 966301394
released: 2015-03-12
updated: 2021-01-21
version: "90.1.0"
score: 4.56792
reviews: 12537
size: 124192768
developerWebsite: 
repository: 
issue: 
icon: com.goabra.abra.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AbraGlobal
providerLinkedIn: company/abra
providerFacebook: GoAbraGlobal
providerReddit: 

redirect_from:

---

This is the iPhone version of the
[Android Abra Bitcoin Crypto Wallet Buy Trade Earn Interest](/android/com.plutus.wallet).

Just like the Android version, this wallet is **not verifiable**.