---
title: "Bitso - Buy and sell bitcoin"
altTitle: 

appId: com.bitso.wallet
idd: 1292836438
released: 2018-02-19
updated: 2020-12-25
version: "2.16.0"
score: 3.82353
reviews: 34
size: 93405184
developerWebsite: https://bitso.com/app
repository: 
issue: 
icon: com.bitso.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

