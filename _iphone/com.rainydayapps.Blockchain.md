---
title: "Blockchain Wallet: Buy Bitcoin"
altTitle: 

appId: com.rainydayapps.Blockchain
idd: 493253309
released: 2012-04-13
updated: 2021-02-06
version: "3.0.2"
score: 4.7444
reviews: 59844
size: 104787968
developerWebsite: https://www.blockchain.com/wallet
repository: https://github.com/blockchain/My-Wallet-V3-iOS
issue: 
icon: com.rainydayapps.Blockchain.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Blockchain
providerLinkedIn: company/blockchain
providerFacebook: Blockchain
providerReddit: 

redirect_from:

---

On the App Store we read:

> Only you have access to your private keys and your crypto.

and on the website there is a link to
[this GitHub account](https://github.com/blockchain/) but no claim about which
repository is supposed to be behind this wallet but we assume it's
[this](https://github.com/blockchain/My-Wallet-V3-iOS).

As with all iPhone app, reproducible builds do not exist so far, so the app
is **not verifiable**.