---
title: "Incognito crypto wallet"
altTitle: 

appId: com.incognito.wallet
idd: 1475631606
released: 2019-08-21
updated: 2021-01-30
version: "4.1.0"
score: 4.24138
reviews: 58
size: 51150848
developerWebsite: https://incognito.org
repository: https://github.com/incognitochain/incognito-wallet
issue: 
icon: com.incognito.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-21
reviewStale: true
signer: 
reviewArchive:


providerTwitter: incognitochain
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This is the iPhone version of [this Android wallet](/android/com.incognito.wallet).

Here we read the same claim as for Android:

> Don’t leave yourself exposed. Go Incognito. It’s non-custodial, decentralized,
  and completely open-source.

and get to the same conclusion about the state of their source code: Without
build instructions and all the files, this app cannot be reproduced and remains
**not verifiable**.
