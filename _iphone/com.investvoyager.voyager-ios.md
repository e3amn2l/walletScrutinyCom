---
title: "Voyager - Buy Bitcoin & Crypto"
altTitle: 

appId: com.investvoyager.voyager-ios
idd: 1396178579
released: 2019-02-13
updated: 2021-01-30
version: "2.9.10"
score: 4.69881
reviews: 11624
size: 58822656
developerWebsite: https://www.investvoyager.com/
repository: 
issue: 
icon: com.investvoyager.voyager-ios.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-02
reviewStale: true
signer: 
reviewArchive:


providerTwitter: investvoyager
providerLinkedIn: company/investvoyager
providerFacebook: InvestVoyager
providerReddit: Invest_Voyager

redirect_from:

---

On their website we read:

> **Advanced Security**<br>
  Offline storage, advanced fraud protection, and government-regulated processes
  keep your assets secure and your personal information safe.

which means this is a custodial offering and therefore **not verifiable**.
