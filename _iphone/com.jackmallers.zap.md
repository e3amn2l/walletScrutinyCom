---
title: "Zap: Bitcoin Lightning Wallet"
altTitle: 

appId: com.jackmallers.zap
idd: 1406311960
released: 2019-04-27
updated: 2020-08-03
version: "0.5.2"
score: 4.97619
reviews: 42
size: 43196416
developerWebsite: https://zaphq.io
repository: https://github.com/LN-Zap/zap-iOS
issue: 
icon: com.jackmallers.zap.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-21
reviewStale: false
signer: 
reviewArchive:


providerTwitter: ln_zap
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This app does not feature a provider website but a GitHub account which links to
[this website](http://zaphq.io).

> **Safe**<br>
  Zap is non-custodial. At no point does anyone have access to funds besides
  you, the user. Your keys, your coins, your node, your rules. All on your
  device.

So we found some code and a claim of not being custodial but the provider does
not claim reproducibility, so we conclude this app is **not verifiable**.
