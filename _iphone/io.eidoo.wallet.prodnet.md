---
title: "Eidoo Ethereum Bitcoin Wallet"
altTitle: 

appId: io.eidoo.wallet.prodnet
idd: 1279896253
released: 2017-09-23
updated: 2021-02-03
version: "2.15.3"
score: 3.91304
reviews: 69
size: 37594112
developerWebsite: https://eidoo.io
repository: 
issue: 
icon: io.eidoo.wallet.prodnet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

