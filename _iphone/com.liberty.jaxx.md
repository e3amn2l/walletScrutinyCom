---
title: "Jaxx Liberty Blockchain Wallet"
altTitle: 

appId: com.liberty.jaxx
idd: 1435383184
released: 2018-10-03
updated: 2021-02-02
version: "2.5.0"
score: 4.5105
reviews: 1095
size: 45841408
developerWebsite: https://jaxx.io
repository: 
issue: 
icon: com.liberty.jaxx.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

