---
title: "Mobile Bitcoin Wallet - Qcan"
altTitle: 

appId: com.qcan.qcan
idd: 1179360399
released: 2017-08-07
updated: 2020-11-12
version: "0.8.848"
score: 4.24138
reviews: 29
size: 91131904
developerWebsite: https://qcan.com
repository: 
issue: 
icon: com.qcan.qcan.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

