---
title: "Bitcoin Wallet: buy BTC & BCH"
altTitle: 

appId: com.bitcoin.mwallet
idd: 1252903728
released: 2017-07-11
updated: 2020-12-15
version: "6.10.4"
score: 4.20288
reviews: 3613
size: 94086144
developerWebsite: https://www.bitcoin.com
repository: 
issue: 
icon: com.bitcoin.mwallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

