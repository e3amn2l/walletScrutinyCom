---
title: "ZenGo: Crypto & Bitcoin Wallet"
altTitle: 

appId: kzencorp.mobile.ios
idd: 1440147115
released: 2019-06-07
updated: 2021-01-18
version: "2.20.0"
score: 4.63121
reviews: 987
size: 69121024
developerWebsite: https://www.zengo.com
repository: 
issue: 
icon: kzencorp.mobile.ios.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

