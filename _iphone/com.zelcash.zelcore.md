---
title: "ZelCore"
altTitle: 

appId: com.zelcash.zelcore
idd: 1436296839
released: 2018-09-23
updated: 2021-01-29
version: "v4.4.0"
score: 4.5625
reviews: 48
size: 62200832
developerWebsite: https://zel.network/zelcore
repository: 
issue: 
icon: com.zelcash.zelcore.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

