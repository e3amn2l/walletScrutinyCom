---
title: "Coinbase – Buy & sell Bitcoin"
altTitle: 

appId: com.vilcsak.bitcoin2
idd: 886427730
released: 2014-06-22
updated: 2021-01-21
version: "9.7.2"
score: 4.6749
reviews: 972937
size: 66922496
developerWebsite: http://www.coinbase.com
repository: 
issue: 
icon: com.vilcsak.bitcoin2.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coinbase
providerLinkedIn: 
providerFacebook: coinbase
providerReddit: 

redirect_from:

---

This app's provider claims:

> Over 98% of cryptocurrency is stored securely offline and the rest is
  protected by industry-leading online security.

which clearly means it is a custodial offering.