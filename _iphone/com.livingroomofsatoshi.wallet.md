---
title: "Wallet of Satoshi"
altTitle: 

appId: com.livingroomofsatoshi.wallet
idd: 1438599608
released: 2019-05-20
updated: 2020-12-16
version: "1.10.4"
score: 4.33333
reviews: 18
size: 32997376
developerWebsite: https://www.walletofsatoshi.com
repository: 
issue: 
icon: com.livingroomofsatoshi.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

