---
title: "BRD Bitcoin Wallet. BTC, Ether"
altTitle: 

appId: org.voisine.breadwallet
idd: 885251393
released: 2014-06-22
updated: 2021-01-13
version: "4.7"
score: 4.58111
reviews: 9370
size: 38657024
developerWebsite: http://brd.com
repository: https://github.com/breadwallet/breadwallet-ios
issue: 
icon: org.voisine.breadwallet.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-21
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BRDHQ
providerLinkedIn: company/brdhq
providerFacebook: brdhq
providerReddit: brdapp

redirect_from:

---

This provider claims

> Trusted by nearly 5 million users in over 170 countries. Nearly $7B USD in
  cryptocurrency under protection.

which is worrying for your privacy cause "how would they know?".

> **Total Bitcoin Cryptocurrency Wallet Privacy**<br>
  No signup is required and BRD connects directly to blockchain networks, not
  BRD’s servers. You are in total control of your privacy and bitcoin crypto
  wallet.

This is a claim which directly implies this app to be non-custodial and they say
it explicitly here:

> **Unhackable Cryptocurrency Security**<br>
  Store all your valuable cryptocurrency in a virtually unhackable environment,
  all for free! Your bitcoin crypto wallet is protected on your own device
  utilizing industry-leading hardware encryption and Apple's mobile security.

Terms like "Unhackable" and "total security" raise our suspicion though. Nothing
is totally secure. Nothing is absolutely unhackable.

But back to the protocol ... So the provider claims the app is non-custodial.
Can we test that? Where is the source code? No link on App Store ...

On their website we find a link to their GitHub and there a repository with
[a promising name](https://github.com/breadwallet/breadwallet-ios).

So is it reproducible? As all iPhone app, we don't see how to reproduce this one
and neither does the provider claim its reproducibility and we give it the
verdict **not verifiable**.