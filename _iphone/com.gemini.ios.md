---
title: "Gemini: Buy Bitcoin Instantly"
altTitle: 

appId: com.gemini.ios
idd: 1408914447
released: 2018-12-11
updated: 2021-02-06
version: "3.2.1"
score: 4.67617
reviews: 11648
size: 107013120
developerWebsite: http://gemini.com
repository: 
issue: 
icon: com.gemini.ios.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: gemini
providerLinkedIn: company/geminitrust
providerFacebook: GeminiTrust
providerReddit: 

redirect_from:

---

This provider being an exchange, together with the lack of clear words of who
gets to hold the private keys leads us to believe this app is only an interface
to the Gemini exchange account and thus custodial and thus **not verifiable**.