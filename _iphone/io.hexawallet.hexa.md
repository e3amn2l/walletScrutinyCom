---
title: "Bitcoin Wallet Hexa"
altTitle: 

appId: io.hexawallet.hexa
idd: 1490205837
released: 2020-03-16
updated: 2021-02-03
version: "1.4.3"
score: 4.75
reviews: 4
size: 47586304
developerWebsite: https://hexawallet.io/
repository: 
issue: 
icon: io.hexawallet.hexa.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

