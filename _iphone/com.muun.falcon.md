---
title: "Muun Wallet"
altTitle: 

appId: com.muun.falcon
idd: 1482037683
released: 2019-10-11
updated: 2021-02-01
version: "2.1.0"
score: 4.86667
reviews: 15
size: 78858240
developerWebsite: https://www.muun.com
repository: https://github.com/muun/falcon
issue: 
icon: com.muun.falcon.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-21
reviewStale: true
signer: 
reviewArchive:


providerTwitter: muunwallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This provider claims:

> Muun is a non-custodial wallet: this means you are in full control of your
  money. You remain in control of your private keys, which are stored only on
  your device, using your phone's secure enclave.

and

> Our code is in an open source repository and can be audited by anyone on the
  Internet.

So they claim the right things and we found
[their source code](https://github.com/muun/falcon) but no claims of
reproducibility so we conclude this app is **not verifiable**.
