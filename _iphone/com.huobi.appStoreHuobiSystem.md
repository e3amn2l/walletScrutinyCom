---
title: "Huobi - Buy & Sell Bitcoin"
altTitle: 

appId: com.huobi.appStoreHuobiSystem
idd: 1023263342
released: 2015-08-19
updated: 2021-02-06
version: "6.1.4"
score: 4.81316
reviews: 2826
size: 211897344
developerWebsite: http://www.hbg.com
repository: 
issue: 
icon: com.huobi.appStoreHuobiSystem.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

