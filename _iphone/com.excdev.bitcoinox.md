---
title: "Bitcoin OX | Crypto Wallet"
altTitle: 

appId: com.excdev.bitcoinox
idd: 1453542836
released: 2019-02-27
updated: 2020-12-13
version: "2.3.5"
score: 5
reviews: 3
size: 52311040
developerWebsite: https://bitcoinox.com/
repository: 
issue: 
icon: com.excdev.bitcoinox.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

