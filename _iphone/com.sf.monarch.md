---
title: "Monarch Wallet"
altTitle: 

appId: com.sf.monarch
idd: 1386397997
released: 2018-06-12
updated: 2020-09-22
version: "1.5.16"
score: 4.79767
reviews: 430
size: 257777664
developerWebsite: 
repository: 
issue: 
icon: com.sf.monarch.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

