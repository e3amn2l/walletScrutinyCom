---
title: "FreeWallet"
altTitle: 

appId: io.freewallet.mobile
idd: 1151168579
released: 2016-11-05
updated: 2019-03-18
version: "1.0.9"
score: 4
reviews: 2
size: 13651968
developerWebsite: https://freewallet.io
repository: https://github.com/jdogresorg/freewallet-mobile
issue: 
icon: io.freewallet.mobile.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-21
reviewStale: false
signer: 
reviewArchive:


providerTwitter: freewallet
providerLinkedIn: 
providerFacebook: freewallet.io
providerReddit: 

redirect_from:

---

In the description we can read:

> Secure<br>
  Wallet Passphrase & private keys never leave device

and

> FreeWallet is an open source mobile wallet which supports Bitcoin.

so it's a non-custodial, open source Bitcoin wallet but can we verify the
claims?

On their website we find a link to their GitHub and from their to the mobile
wallet's repository. There we find no claims of reproducibility and not even
build isntructions, so this app is **not verifiable**.
